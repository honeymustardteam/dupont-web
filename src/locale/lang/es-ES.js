export default {
  loginBase:{
    iniciarsesion: 'Iniciar Sesión',
    correo: 'Correo electrónico',
    contrasenia: 'Contraseña',
    ingresar: 'Ingresar',
    olvide_contrasenia: 'Olvidé mi contraseña',
    registrar_nuevo: 'Registrar nuevo usuario',
  },
  registro:{
    titulo: 'INFORMACIÓN DE CONTACTO',
    nombre_completo: 'Nombre Completo',
    nombre_empresa: 'Nombre de la Empresa',
    funcion:'Función',
    correo:'Correo Electrónico',
    contrasenia:'Contraseña',
    repetir_contrasenia:'Repetir Contraseña',
    registro:'REGISTRO',
    regresar:'REGRESAR',
  },
  sidebar:{
    inicio: 'INICIO',
    mis_prototipos: 'MIS PROTOTIPOS',
    select_prototipo: 'PROTOTIPO',

    nuevo_prototipo:  'NUEVO PROTOTIPO',
    especificaciones: 'ESPECIFICACIONES',
    tipo_contenedor: 'TIPO DE CONTENEDOR',
    num_piezas: 'NÚMERO DE PIEZAS (SNP)',
    tipo_uso: 'TIPO DE USO',
    almacenamiento: 'ALMACENAMIENTO',
    dunnage: 'DUNNAGE',

    salir: 'SALIR',
  },
  home:{
    bienvenido: 'BIENVENIDO',
    nuevo_prototipo: 'Nuevo Prototipo de Empaque',
    ya_existe_prototipo: 'Ya estás editando un prototipo',
    deseas_crear: '¿Deseas crear uno nuevo?',
    btn_crear_nuevo: 'Crear nuevo',
    btn_continuar_editando: 'Continuar editando'
  },
  mis_prototipos:{
    mis_prototipos: 'Mis Prototipos',
    prototipo: 'Prototipo',
    nuevo_prototipo: 'Nuevo Prototipo de Empaque'
  },
  continuar: 'CONTINUAR',
  regresar: 'REGRESAR',
  en_colaboracion: 'EN COLABORACIÓN CON',
  especificaciones:{
    peso:'Peso',
    gramos: '(Gramos)',
    paso1: {
      titulo: 'Información del Proyecto',
      subtitulo: 'Completa la información',
    },
    paso2:{
      titulo: 'Tipo de Clase',
      subtitulo: 'Selecciona un tipo de Clase',
    },
    paso3:{
      exterior: 'EXTERIOR',
      interior: 'INTERIOR',
    },
    numero_prototipo: 'Número del Prototipo',
    nuevo_prototipo: 'Nuevo Prototipo',
    info_proyecto: '',
    completa_info: '',
    nombre_proyecto: 'Nombre del Proyecto',
    plataforma: 'Plataforma',
    modelo: 'Modelo',
    piezas_totales: 'Piezas Totales del Proyecto',
    fecha: 'Fecha de Inicio',
    clase: 'CLASE',
    tipo_clase:{
      a: {
        1: 'Piezas visibles con superficies estéticas y/o con texturas especiales.',
        2: 'Piezas proporcionadas por el cliente (OEM).',
        3: 'Piezas sin bisagras, herrajes, ranuras, acanalados, etc.',
        4: 'Piezas hechas por diferentes softwares.',
        5: 'Piezas con curvaturas continuas.',
        // 6: 'Superficies que tienen continuidad de curvas se llaman Clase “A”',
      },
      b: {
        1: 'Piezas visibles y/o no visibles.',
        2: 'Piezas en su mayoría sin texturas.',
        3: 'Piezas con superficies que tienen que ser desarrolladas basadas en una clase A.',
        4: 'Piezas con bisagras, herrajes, ranuras, acanalado, etc.',
        // 5: 'Piezas con curvaturas continuas.',
        // 6: 'Superficies que tienen continuidad de curvas se llaman Clase “A”',
      },
      c: {
        1: 'Piezas ocultas.',
        2: 'Piezas metal-mecánicas.',
        3: 'Piezas estructurales.',
        4: 'Piezas con superficies sin tratamiento especial.'
      }
    },
    ubicacion:{
      ubicacion: 'Ubicación de la pieza',
      selecciona: 'Selecciona donde se encuentra la pieza',
      exterior: 'EXTERIOR',
      interior: 'INTERIOR'
    },
    pieza: {
      pieza: 'Pieza',
      selecciona: 'Selecciona donde se encuentra la pieza',
      interior:{
        clase:{
          c: {
            puertas: 'Insertos de puertas',
            consola: 'Consola',
            guantera: 'Guantera',
            conductos: 'Conductos de Aire',
            cerraduras: 'Cerraduras de Plástico',
            piano: '"Piano Black"',
            naunced: 'Naunced',
            acabados: 'Acabados Especiales',
            descansa: 'Descanzabrazos',
            vista: 'Vista de grano',
            pilares: 'Pilares de la Puerta',
            tronco: 'Maletero, Plástico, Texturado',
            descansa_2: 'Descansabrazos, Plástico texturados'
          },
          b: {
            puertas: 'Insertos de puerta',
            consola: 'Consola',
            piano: '"Piano Black"',
            naunced: 'Naunced',
            acabados: 'Acabados Especiales',
            descansa: 'Descansabrazos',
            vista: 'Vista de grano',
            plasticos: 'Plásticos texturados',
            maletero: 'Maletero'


          }
        }
      },
      exterior:{
        clase:{
          a: {
            titulo: 'Clase',
            headlights: 'Faros, luces traseras, carcasas y elementos ópticos',
            plastic_paint: 'Plásticos pintados y con acabados especiales',
            leather: 'Cuero, vinilo, telas no tejidas, vestiduras',
            roof: 'Quemacocos, marcos de Ventanas, Cristal, Sellado',
            plastics: 'Pintados, piezas metálicas o cromadas'
          },
          c: {
            titulo: 'Clase',
            leds: 'LEDs',
            parts: 'Partes Metálicas o de aleación bordes con forma. Piezas sin acabados especiales troquelados',
            tarjetas: 'Tarjetas madre. Circuitos. Procesadores',
            arnes: 'Arneses y cableados automotrices',
            plastics: 'Pintados, piezas metálicas o cromadas'
          }
        }
      }//fin exterior
    }, //Fin pieza
    forma_pieza:{
      forma_pieza: 'De acuerdo a la forma de tu pieza',
      sub: '¿Cúal es el perímetro que mejor se adapta?',
      trapezoide: 'Trapezoide',
      rectangular: 'Rectangular',
      cuadrada: 'Cuadrada',
      triangular: 'Triangular',
      txt: 'La forma de su pieza desde la vista superior o lateral es',
      view: ''
    },
    dimenciones: 'Dimensiones de la pieza',
    acabado:{
      acabado: 'Acabado de',
      sub: 'Superficie de contacto',
      opciones:{
        inyecciones: 'Inyecciones',
        pintadas: 'Pintadas',
        acabados: 'Acabados especiales',
      }
    }

  }, //Fin Especificaciones
  tipo_contenedor:{
    tipo_contenedor: 'Tipo de Contenedor',
    sub: 'Selecciona el tipo de contenedor donde almacenarás la pieza',
    otros: 'Otros'
  },
  num_piezas:{
    num_piezas: 'Número de Piezas por Contenedor (SNP)'
  },
  tipo_uso:{
    tipo_uso:'Traslado de las piezas',
    como_vas: '¿Cómo vas a usar el contenedor?',
    externo: 'Externo (Movible) ',
    interno: 'Interno (WIP)',
    no_seguro: 'No estoy seguro'
  },
  almacenamiento: {
    almacenamiento: 'Almacenamiento',
    donde_se: 'Dónde vas a almacenar el contenedor',
    interior: 'Interior',
    exterior: 'Exterior',
    no_seguro: 'No estoy seguro',
  },
  dunnage:{
    rigido: 'Rígido',
    disipativo: 'Disipativo (ESD)',
    not: 'No estoy seguro',
    bicapa: 'Bi-capa',
    tricapa: 'Tri-capa'
  },
  resumen:{
    resumen: 'Resumen del Prototipo',
    deacuerdo_a: 'De acuerdo a lo que elegiste, tu prototipo es este',
    clase: 'Clase',
    forma: 'Forma',
    estados:{
      pendiente: 'Pendiente',
      cotizacion: 'En Cotización',
      produccion: 'En Producción',
      entregado: 'Entregado'
    },
    dimenciones: 'Dimensiones',
    peso : 'Peso',
    acabado: 'Acabado',
    contenedor: 'Contenedor',
    de_acuerdo1: 'De acuerdo al número de piezas que ingresaste, requieres',
    de_acuerdo2: 'contenedor(es)',
    aviso:'Las medidas de tu pieza sobrepasan la capacidad del contenedor, sugerimos contactar a un asesor.',
    cantidad_piezas: 'Cantidad de Piezas',
    cantidad_piezas_2: 'Cantidad de piezas del proyecto',
    pzaspcontenedor: 'por contenedor',
    tipo_uso: 'Tipo de Uso',
    almacenamiento: 'Almacenamiento',
    material: 'Material',
    tipo: 'Tipo',
    solicitar_1:'Solicitar solo diseño del prototipo',
    solicitar_2:'Solicitar Prototipo',
    editar: 'Editar Prototipo',
    formulario_solicitar_pdf: 'Solicitar PDF',
    formulario_num_telefonico: 'Número Telefonico',
    formulario_entidad_federativa: 'Entidad Federativa',
    formulario_alcaldia_municipio: 'Alcaldía / Municipio',
    formulario_colonia: 'Colonia',
    formulario_calle: 'Calle',
    formulario_cp: 'CP'
  }
}
