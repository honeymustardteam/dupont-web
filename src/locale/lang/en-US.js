export default {
  loginBase:{
    iniciarsesion: 'Log in',
    correo: 'Email',
    contrasenia: 'Password',
    ingresar: 'Enter',
    olvide_contrasenia: 'Forget Password',
    registrar_nuevo: 'Create a new account',
  },
  registro:{
    titulo: 'USER DATA',
    nombre_completo: 'Full Name',
    nombre_empresa: 'Company Name',
    funcion:'Title',
    correo:'Email',
    contrasenia:'Password',
    repetir_contrasenia:'Repeat Password',
    registro:'Sign in',
    regresar:'BACK',
  },
  sidebar:{
    inicio: 'HOME',
    mis_prototipos: 'MY PROTOTYPES',
    select_prototipo: 'PROTOTYPE',

    nuevo_prototipo:  'NEW PROTOTYPE',
    especificaciones: 'SPECIFICATIONS',
    tipo_contenedor: 'CONTAINER TYPE',
    num_piezas: 'NUMBER OF PARTS (SNP)',
    tipo_uso: 'TYPE OF USE',
    almacenamiento: 'STORAGE',
    dunnage: 'DUNNAGE',

    salir: 'LOG OUT',
  },
  home:{
    bienvenido: 'Welcome',
    nuevo_prototipo: 'New Packaging Prototype', 
    ya_existe_prototipo: 'You are already editing a prototype',
    deseas_crear:'Do you want to create a new one?',
    btn_crear_nuevo: 'Create new',
    btn_continuar_editando: 'Continue editing'
  },
  mis_prototipos:{
    mis_prototipos: 'My Prototypes',
    prototipo: 'Prototype',
    nuevo_prototipo: 'New Packaging Prototype'
  },
  continuar: 'CONTINUE',
  regresar: 'RETURN',
  en_colaboracion: 'IN COLLABORATION WITH',
  especificaciones:{
    peso:'Weight',
    gramos: '(Grams)',
    paso1: {
      titulo: 'Project Data',
      subtitulo: 'Complete the information',
    },
    paso2:{
      titulo: 'Class Type',
      subtitulo: 'Select a class type',
    },
    paso3:{
      exterior: 'EXTERIOR',
      interior: 'INSIDE',
    },
    numero_prototipo: 'Prototype Number',
    nuevo_prototipo: 'New Prototype',
    info_proyecto: '',
    completa_info: '',
    nombre_proyecto: 'Project name',
    plataforma: 'Platform',
    modelo: 'Model',
    piezas_totales: 'Project´s Total Items',
    fecha: 'Start Date',
    clase: 'Class',
    tipo_clase:{
      a: {
        1: 'Visible pieces with aesthetic surfaces and / or special textures.',
        2: 'Pieces provided by the customer (oem).',
        3: 'Pieces without hinges, ironwork, grooves,etc.',
        4: 'Pieces made by different softwares.',
        5: 'Pieces with continuous curvatures.',
      },
      b: {
        1: 'Non-visible parts.',
        2: 'Pieces mostly without textures.',
        3: 'Pieces without surfaces that must be developed based on “a” type.',
        4: 'Pieces without hinges, ironwork, grooves,etc.',
        // 5: 'Piezas con curvaturas continuas.',
        // 6: 'Superficies que tienen continuidad de curvas se llaman Clase “A”',
      },
      c: {
        1: 'Hidden pieces.',
        2: 'Metal-mechanical piece.',
        3: 'Structural piece.',
        4: 'Pieces with surfaces without special tratement.'
      }
    },
    ubicacion:{
      ubicacion: 'Piece Location',
      selecciona: 'Select piece location',
      exterior: 'EXTERIOR',
      interior: 'INTERIOR'
    },
    pieza: {
      pieza: 'Piece',
      selecciona: 'Select where the piece is found',
      interior:{
        clase:{
          c: {
            puertas: 'Door Inserts',
            consola: 'Console',
            guantera: 'Gloves Case',
            conductos: 'Air Conduct',
            cerraduras: 'Plastic Locks',
            piano: 'Pianoblack',
            naunced: 'Naunced',
            acabados: 'Special Finishes',
            descansa: 'Arm Rest',
            vista: 'Grain View',
            pilares: 'Door Pillars',
            tronco: 'Texturized plastic trunk',
            descansa_2: 'Arm rest with plastic texture'
          },
          b: {
            puertas: 'Door Inserts',
            consola: 'Console',
            piano: '"Piano Black"',
            naunced: 'Naunced',
            acabados: 'Special Finishes',
            descansa: 'Arm Rest',
            vista: 'Grain View',
            plasticos: 'Textured plastic',
            maletero: 'Trunk'



          }
        }
      },
      exterior:{
        clase:{
          a: {
            titulo: 'Class',
            headlights: 'Headlights, tail lights, housings and optical elements',
            plastic_paint: 'Plastic with paint & special finish',
            leather: 'Leather, Vinyl, Nylon wovens, car clothes',
            roof: 'Sun Roof, window quarter, glass, sealing',
            plastics: 'Painting, Metallic parts or chrome finish',
          }
        }
      }//fin exterior
    }, //Fin pieza
    forma_pieza:{
      forma_pieza: 'According your piece´s shape,',
      sub: 'What is the best perimeter?',
      trapezoide: 'Trapezoid',
      rectangular: 'Rectangle',
      cuadrada: 'Square',
      triangular: 'Triangular',
      txt: 'Piece´s shape top / Side view',
      view: 'view'
    },
    dimenciones: 'Piece´s Dimensions',
    acabado:{
      acabado: 'Surface´s texture of the piece',
      sub: 'Finish',
      opciones:{
        inyecciones: 'Injections',
        pintadas: 'Painted',
        acabados: 'Special finishes',
      }
    }
  }, //Fin Especificaciones
  tipo_contenedor:{
    tipo_contenedor: 'Container Type',
    sub: "Select the container type for the pieces.",
    otros: 'Others'
  },
  num_piezas:{
    num_piezas: 'Pieces per container (SNP)'
  },
  tipo_uso:{
    tipo_uso:'Transfer of Parts',
    como_vas: 'Container´s use',
    externo: 'External (Mobile)',
    interno: 'Internal (WIP)',
    no_seguro: 'Unknown'
  },
  almacenamiento: {
    almacenamiento: 'Storage',
    donde_se: 'Where you will store the container?',
    interior: 'Internal (WIP)',
    exterior: 'External (Mobile)',
    no_seguro: 'Unknown',
  },
  dunnage:{
    rigido: 'Rigid',
    disipativo: 'Dissipative (ESD)',
    not: 'I am not sure',
    bicapa: 'Bi-Layer',
    tricapa: 'Tri-Layer'

  },
  resumen:{
    resumen: 'Summary',
    deacuerdo_a: 'According your choose, your prototype is this',
    clase: 'Type',
    forma: 'Shape',
    estados:{
      pendiente: 'Pending',
      cotizacion: 'In quotation',
      produccion: 'In production',
      entregado: 'Delivered'
    },
    dimenciones: 'Dimensions',
    peso : 'Weight',
    acabado: 'Texture',
    contenedor: 'Container',
    de_acuerdo1: 'According to the number of pieces you entered, you require',
    de_acuerdo2: 'containers',
    aviso:'The measurements of your piece go over the capacity of your container, we suggest you to contact an adviser.',
    cantidad_piezas: 'Pieces',
    cantidad_piezas_2: 'Amount of parts of project',
    pzaspcontenedor: 'per container',
    tipo_uso: 'Type of use',
    almacenamiento: 'Storage',
    material: 'Material',
    tipo: 'Type',
    solicitar_1:'Request only Prototype Design',
    solicitar_2:'Request Prototype',
    editar: 'Edit Prototype',
    formulario_solicitar_pdf: 'Request PDF',
    formulario_num_telefonico: 'Phone Number',
    formulario_entidad_federativa: 'State',
    formulario_alcaldia_municipio: 'City',
    formulario_colonia: 'Suburb',
    formulario_calle: 'Street',
    formulario_cp: 'Zip Code'
  }
}
