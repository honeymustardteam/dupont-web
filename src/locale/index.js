import Vue from 'vue'
import VueI18n from 'vue-i18n'
import { localRead } from '@/libs/util'
import customEnUs from './lang/en-US'
import customEsEs from './lang/es-ES'

Vue.use(VueI18n)

//es-ES
const navLang = navigator.language
const localLang = (navLang === 'es-ES' || navLang === 'en-US') ? navLang : false
let lang = localLang || localRead('local') || 'es-ES'

Vue.config.lang = lang

// vue-i18n 6.x+写法
Vue.locale = () => {}
const messages = {
  'en-US': customEnUs,
  'es-ES': customEsEs
}
const i18n = new VueI18n({
  locale: lang,
  messages
})

export default i18n
