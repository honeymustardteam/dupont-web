const paginacionMixins = {
    data(){
        return {
            currentPage: 1,
            perPage: 10,
            totalRows: 0, //items.length,
            pageOptions: [5, 10, 15],
            sortBy: null,
            sortDesc: false,
            sortDirection: "asc",
            filter: null,
        }
    },
    methods:{
        onFiltered(filteredItems) {
            this.totalRows = filteredItems.length;
            this.currentPage = 1;
          },
    }
}

export {
    paginacionMixins
}