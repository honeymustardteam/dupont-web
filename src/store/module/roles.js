import firebase from '@/firebase/init'
const db = firebase.firestore()

var cargadorListaRoles = null;
var cargadorRol = null;

const store = {
  state: {
    rol:{},
    roles:{},
    rolHeader:{},
  },
  getters: {
    rol: state => {
      return state.rol;
    },
    rolHeader: state => {
      return state.rolHeader;
    },
    rolesConNotificacion: state => {
      //this.rolHeader.modulos.todos_proveedores.informacion.consultar
      var itemRoles = [];

      for (var key in state.roles) {
          var item = state.roles[key];

          if(item.modulos.notificaciones.informacion.consultar){
            itemRoles.push(item);
          }
          
      }

      return itemRoles;
    },
    roles: state => {
      return state.roles;
    },
  },
  mutations: {
    ASIGNA_ROL(state, data) {
      state.rol= data;
    },
    ASIGNA_ROL_HEADER(state, data) {
      state.rolHeader= data;
    },
    ASIGNA_ROLES(state, data) {
      state.roles= data;
    },
  },
  actions: {

    agregarRol({ dispatch, getters }, rol) {
      return new Promise((resolve, reject) => {
        db.collection("Roles")
        .add(rol)

          resolve()
      })
    },

    actualizarRol({ dispatch, getters }, rol) {
       return new Promise((resolve, reject) => {
        db.collection("Roles")
           .doc(rol.id)
           .update(rol)

           resolve()
       })
     },


    eliminarRol({getters}, idRol) {
      return db.collection("Roles")
        .doc(idRol)
        .delete()
    },



    /**CARGAR ROL SELECCIONADO */
    cargarRol({commit}, idRol){
        return new Promise((resolve, reject) => {
          try {
            var docRef = db.collection("Roles").doc(idRol);
            docRef.get().then(function(doc) {
                if (doc.exists) {
                    commit('ASIGNA_ROL', doc.data())
                    resolve(doc.data())
                } else {
                    reject("No such document!")
                }
            }).catch(function(error) {
                console.log("Error getting document:", error);
                reject(error)
            });
          } catch (error) {
            reject(error)
          }
        })
      },

    cargarRolFront({ commit,getters }, idRol) {
      return new Promise((resolve, reject) => {
        if (cargadorRol === null) {
          let docPath = db.collection("Roles").doc(idRol);
          cargadorRol = docPath.onSnapshot(doc => {
            commit('ASIGNA_ROL_HEADER', doc.data())
            resolve(doc.data())
          })
        } else {
          resolve()
        }
      })
    },


    /** Cargar lista de roles para la tabla */
    getListaRoles({ commit }) {
        return new Promise((resolve, reject) => {
          if (cargadorListaRoles === null) {
  
            cargadorListaRoles = db.collection("Roles")
            //.where("activo", "==", true)
            .onSnapshot(function(querySnapshot) {
                var roles = [];
                querySnapshot.forEach(function(doc) {
                  let data = doc.data();
                  data.id = doc.id;
                  roles.push(data);
                });
                commit('ASIGNA_ROLES', [])
                commit('ASIGNA_ROLES', roles)
                resolve(roles)
            });
  
          } else {
            resolve()
          }
        })
      },
    
   
    

  }
}

export default store
