var baseUrl = window.location.origin;
import firebase from '@/firebase/init'
const db = firebase.firestore()

import { setToken, getToken } from '@/libs/util'


var cargadorUsuarios = null

const store = {
  state: {
    hasGetInfo: false,
    token: getToken(),
    access: '',
    infoUser: null,
    infoUserRealTime: null,
  },
  getters: {
    infoUser: state => {
      return state.infoUser
    },
    infoUserRealTime: state => {
      return state.infoUserRealTime
    },
  },
  mutations: {
    setInfoUser (state, data) {
      state.infoUser = data
    },
    setInfoUserRealTime(state, data) {
      state.infoUserRealTime = data
    },
    setHasGetInfo (state, status) {
      state.hasGetInfo = status
    },
    setAccess (state, access) {
      state.access = access
    },
    setToken (state, token) {
      state.token = token
      setToken(token)
    },
  },
  actions: {
    /*getUsuario({commit}){
      db.collection('Usuarios').doc('GVADwxoqYFfibvI49g8v').get()
      .then( doc =>{
        let usuario = doc.data();
        commit('setInfoUser',usuario)
      })
    },*/
    getUserInfoRealTime ({ state, commit }) {
      return new Promise((resolve, reject) => {
        try {
          if (cargadorUsuarios === null) {
            let usuarioLogeado = null;
            cargadorUsuarios = db.collection("Usuarios").where("token", "==", state.token)
            .onSnapshot(function(querySnapshot) {
                querySnapshot.forEach(function(doc) {
                    usuarioLogeado = doc.data();
                    
                });
                //commit('setToken', usuarioLogeado.token)
                commit('setInfoUserRealTime', usuarioLogeado)
                resolve(usuarioLogeado)
            });
          } else {
            resolve()
          }

        } catch (error) {
          reject(error)
        }
      })
    },


    getUserInfo ({ state, commit }) {
      return new Promise((resolve, reject) => {
        try {
          let usuarioLogeado = null;
          db.collection("Usuarios").where("token", "==", state.token)
          .get()
          .then(function(querySnapshot) {
              querySnapshot.forEach(function(doc) {
                  usuarioLogeado = doc.data();
                  
              });
              //commit('setToken', usuarioLogeado.token)
              commit('setInfoUser', usuarioLogeado)
              if(usuarioLogeado !== null ){commit('setAccess', usuarioLogeado.access)}
              commit('setHasGetInfo', true)
              resolve(usuarioLogeado)
          })
          .catch(function(error) {
              reject(error)
              console.log("Error getting documents: ", error);
          });

        } catch (error) {
          reject(error)
        }
      })
    },


    loginUser({commit}, payload){
      return new Promise((resolve, reject) => {
        try {
          let usuarioLogeado = null;
          var loginUser = db.collection("Usuarios");
          loginUser.where("correo", "==", payload.correo)
          .where("activo", "==", true)
          .where("validado", "==", true)
          .where("contrasenia", "==", payload.contrasenia)
          .get()
          .then(function(querySnapshot) {
              querySnapshot.forEach(function(doc) {
                  usuarioLogeado = doc.data();
                  usuarioLogeado.token = doc.id;
              });
              if(usuarioLogeado !== null) commit('setToken', usuarioLogeado.token)
              console.log("TCL: loginUser -> usuarioLogeado", usuarioLogeado)
              commit('setInfoUser', usuarioLogeado)

              resolve(usuarioLogeado)
            })
            .catch(function(error) {
                reject(error)
                console.log("Error getting documents: ", error);
            });
        } catch (error) {
          reject(error)
        }
    })    

    }, /**Fin loginUser */
    handleLogOut ({ commit }) {
      return new Promise((resolve) => {
        commit('setToken', '')
        commit('setAccess', [])
        resolve('cerrado')
      })
    },
    
  } /**Fin actions */
} /**Fin store */

export default store
