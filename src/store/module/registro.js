var baseUrl = window.location.origin;

import firebase from '@/firebase/init'
const db = firebase.firestore()
const storage = firebase.storage().ref()

var cargadorListaResponsables = null
var cargadorListaClientes = null


const store = {
  state: {
    listaResponsables: [],
    listaClientes: [],
  },
  getters: {
    listaResponsables: state => {
      return state.listaResponsables;
    },
    listaClientes: state => {
      return state.listaClientes;
    },
  },
  mutations: {
    setListaResponsables (state, payload) {
      state.listaResponsables = payload
    },
    setListaClientes(state, payload) {
      state.listaClientes = payload
    },
  },
  actions: {
    addRegistro({commit, dispatch}, payload){

      return new Promise((resolve, reject) => {
        try {
          db.collection("Usuarios").add(payload.info)
          .then(function(docRef) {
              console.log("Document written with ID: ", docRef.id);
              var data = {
                info: {
                  token: docRef.id, 
                }
              }
              dispatch('actualizarRegistro',data).then(res => {
                resolve(docRef)
              });
              
          })
          .catch(function(error) {
              console.error("Error adding document: ", error);
              reject(error)
          });
        } catch (error) {
          reject(error)
        }
      })
    },


    getListaResponsables({ commit }, idUser) {
      return new Promise((resolve, reject) => {
        if (cargadorListaResponsables === null) {

          cargadorListaResponsables = db.collection("Usuarios")
          .where("es_registrado", "==", false)
          .onSnapshot(function(querySnapshot) {
              var responsables = [];
              querySnapshot.forEach(function(doc) {
                let data = doc.data();
                data.id = doc.id;
                responsables.push(data);
              });
              commit('setListaResponsables', [])
              commit('setListaResponsables', responsables)
              resolve(responsables)
          });

          //commit('setListaResponsables',tareas)


        } else {
          resolve()
        }
      })
    },

    getListaClientes({ commit }, idUser) {
      return new Promise((resolve, reject) => {
        if (cargadorListaClientes === null) {

          cargadorListaClientes = db.collection("Usuarios")
          .where("es_registrado", "==", true)
          .onSnapshot(function(querySnapshot) {
              var clientes = [];
              querySnapshot.forEach(function(doc) {
                let data = doc.data();
                data.id = doc.id;
                clientes.push(data);
              });
              commit('setListaClientes', [])
              commit('setListaClientes', clientes)
              resolve(clientes)
          });

          //commit('setListaClientes',tareas)


        } else {
          resolve()
        }
      })
    },


    getRegistro ({},id) {
      return new Promise((resolve, reject) => {
        try {
          var docRef = db.collection("Usuarios").doc(id);
          docRef.get().then(function(doc) {
              if (doc.exists) {
                  resolve(doc.data())
              } else {
                  // doc.data() will be undefined in this case
                  console.log("No such document!");
                  resolve(doc.data())
              }
          }).catch(function(error) {
              console.log("Error getting document:", error);
              reject(error)
          });

        } catch (error) {
          reject(error)
        }
      })
    },

    buscarEmail ({}, correo) {
      return new Promise((resolve, reject) => {
        try {
          let existeCorreo = false;
          db.collection("Usuarios").where("correo", "==", correo)
          .get()
          .then(function(querySnapshot) {
              querySnapshot.forEach(function(doc) {
                //doc.data();
                existeCorreo = doc.data();
                  
              });
              resolve (existeCorreo)
          })
          .catch(function(error) {
              reject(error)
              console.log("Error getting documents: ", error);
          });

        } catch (error) {
          reject(error)
        }
      })
    },

    actualizarRegistro ({dispatch}, payload) {
      return new Promise((resolve, reject) => {
        try {
          var docRef = db.collection("Usuarios").doc(payload.info.token);
          docRef.update(payload.info)
          .then(function() {
              console.log("Document successfully updated!");
                resolve('actualizado')
          })
          .catch(function(error) {
              // The document probably doesn't exist.
              console.error("Error updating document: ", error);
              reject(error)
          });

        } catch (error) {
          reject(error)
        }
      })
    },
    subirLogotipo({}, archivo) {
      return new Promise((resolve, reject) => {
        let nombreCarpeta  = Date.now();
        storage
          .child(`Uploads/Proovedores/${nombreCarpeta}/${archivo.id_registro}/${archivo.name}`)
          .put(archivo)
          .then(uploadTaskSnapshot => {
            uploadTaskSnapshot.ref
              .getDownloadURL()
              .then(url => {
                resolve({url: url, nombreCarpeta:''+nombreCarpeta })
              })
              .catch(error => reject(error))
          })
          .catch(error => reject(error))
      })
    },

    eliminarLogotipo({}, url) {
      return new Promise((resolve, reject) => {
        var storageRef = storage.child('Uploads/Proovedores/' + url)
        storageRef
          .delete()
          .then(resp => {
            resolve(resp)
          })
          .catch(error => {
            reject(error)
          })
      })
    },


    
  } /**Fin actions */

} /**Fin store */

export default store
