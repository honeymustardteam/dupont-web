var baseUrl = window.location.origin;
import firebase from '@/firebase/init'
const db = firebase.firestore()
const storage = firebase.storage().ref()

var cargadorListaProtos = null
var carListaProtoSolicitados = null
var carListaProtoSolicitadosPorResponsable = null

const store = {
  state: {
    listaProtos: [],
    listaProtoSolicitados: [],
    prototipo: {
      user_id: null,
      responsable: false,
      completado: false,
      especificaciones:{
          completado: false,
          pasos_completados: {
            paso1: false,
            paso2: false,
            paso3: false,
            paso4: false,
            paso5: false,
            paso6: false,
            paso7: false,
            paso8: false,
          },

          //PASO 1
          numero_prototipo: null,
          nombre_proyecto: null,
          plataforma: null,
          modelo: null,
          pzasTotales: null,
          fecha: '',

          //PASO 2
          tipoClase: '',

          //PASO 3
          ubicacion_pieza: null,

          //PASO 4
          pieza_interior: '',
          pieza_exterior: '',

          //PASO 5
          forma_pieza: '',

          //paso 6
          altura_pieza: 1,
          ancho_pieza: 1,
          largo_pieza: 1,

          //paso 7 peso
          peso_num: 0,
          es_kg: true,

          //paso 8 acabado
          acabado: 'Graining',

      },
      tipo_contenedor:{
          completado: false,
          tipo_contenedor: null,
          otro:{
            altura: null,
            ancho: null,
            largo: null
          }
      },
      num_piezas:{
          completado: false,
          num_piezas: 1,
      },
      tipo_uso:{
        tipo_uso: null,
        completado: false,
      },
      almacenamiento:{
        almacenamiento: null,
        completado: false,
      },
      dunnage:{
          completado: false,
          dunnageR: null,
          colorM: '',
          acabado: '',
          nivel4: '',
          nivel3: '',
          nivel2: '',
          nivel1: '',
      },
      solicitar_btn:{
        disenio: false,
        prototipo: false,
      },
      solicitar_disenio: false,
      solicitar: false,
      estatus: 'Pendiente',
      activo: true,
    },
  },
  getters: {
    listaProtos: state => {
      return state.listaProtos;
    },
    listaProtoSolicitados: state => {
      return state.listaProtoSolicitados;
    }, 
    prototipo: state => {
      return state.prototipo;
    },
  },
  mutations: {
    setListaProtos (state, payload) {
      state.listaProtos = payload
    },
    setListaSolicitudes (state, payload) {
      state.listaProtoSolicitados = payload
    },
    setPrototipo(state, payload) {
      state.prototipo = payload
    },
  },
  actions: {
    
    getListaProtos({ commit }, idUser) {
      return new Promise((resolve, reject) => {
        if (cargadorListaProtos === null) {

          cargadorListaProtos = db.collection("Prototipos")
          .where("user_id", "==", idUser)
          .onSnapshot(function(querySnapshot) {
              var prototipos = [];
              querySnapshot.forEach(function(doc) {
                let data = doc.data();
                data.id = doc.id;
                prototipos.push(data);
              });
              commit('setListaProtos', [])
              commit('setListaProtos', prototipos)
              resolve(prototipos)
          });

          //commit('setListaProtos',tareas)


        } else {
          resolve()
        }
      })
    },

    // MMUESTRA TODAS LAS SOLICITUDES PARA QUE VEA EL ADMIN
    getListaProtosSolicitados({ commit }, idUser) {
      return new Promise((resolve, reject) => {
        if (carListaProtoSolicitados === null) {

          carListaProtoSolicitados = db.collection("Prototipos")
          .where("solicitar", "==", true)
          .onSnapshot(function(querySnapshot) {
              var solicitudes = [];
              querySnapshot.forEach(function(doc) {
                let data = doc.data();
                data.id = doc.id;
                solicitudes.push(data);
              });
              commit('setListaSolicitudes', [])
              commit('setListaSolicitudes', solicitudes)
              resolve(solicitudes)
          });

        } else {
          resolve()
        }
      })
    },

    // MMUESTRA TODAS LAS SOLICITUDES PARA QUE VEA EL RESPONSABLE POR ID
    getLisProtoNoAdmin({ commit }, idUser) {
      return new Promise((resolve, reject) => {
        if (carListaProtoSolicitadosPorResponsable === null) {

          carListaProtoSolicitadosPorResponsable = db.collection("Prototipos")
          .where("solicitar", "==", true)
          .where("responsable", "==", idUser)
          .onSnapshot(function(querySnapshot) {
              var solicitudes = [];
              querySnapshot.forEach(function(doc) {
                let data = doc.data();
                data.id = doc.id;
                solicitudes.push(data);
              });
              commit('setListaSolicitudes', [])
              commit('setListaSolicitudes', solicitudes)
              resolve(solicitudes)
          });

        } else {
          resolve()
        }
      })
    },


    getUsuario ({},id_usuario) {
      console.log("TCL: getUsuario -> id_usuario", id_usuario)
      return new Promise((resolve, reject) => {
        try {
          var docRef = db.collection("Usuarios").doc(id_usuario);
          docRef.get().then(function(doc) {
              if (doc.exists) {
                  resolve(doc.data())
              } else {
                  // doc.data() will be undefined in this case
                  console.log("No such document!");
                  resolve(null)
              }
          }).catch(function(error) {
              console.log("Error getting document:", error);
              reject(error)
          });

        } catch (error) {
          reject(error)
        }
      })
    },

    setPrototipo({commit}, payload){
      commit('setPrototipo', payload)
    },

    cleanPrototipo({commit}, payload){
      commit('setPrototipo', payload.PROTOTIPO)
    },
    
    addPrototipo({dispatch}, payload){
      return new Promise((resolve, reject) => {
        try {
          db.collection("Prototipos").add(payload)
          .then(function(docRef) {
              console.log("Document written with ID: ", docRef.id);
              
              let data = {
                token: docRef.id,
                id: docRef.id
              }
              dispatch('updatePrototipo',data).then(res => {
                  resolve(docRef)
              });
          })
          .catch(function(error) {
              console.error("Error adding document: ", error);
              reject(error)
          });
        } catch (error) {
          reject(error)
        }
      })
    },

    updatePrototipo({}, payload){
      console.log("TCL: updatePrototipo -> payload", payload)
      return new Promise((resolve, reject) => {
        try {
          var docRef = db.collection("Prototipos").doc(payload.id);
          docRef.update(payload)
          .then(function() {
              console.log("Document successfully updated!");
              resolve('actualizado')
          })
          .catch(function(error) {
              // The document probably doesn't exist.
              console.error("Error updating document: ", error);
              reject(error)
          });

        } catch (error) {
          reject(error)
        }
      })
    },

    subirFotoID({}, archivo) {
      return new Promise((resolve, reject) => {
        let nombreCarpeta  = Date.now();
        storage
          .child(`Uploads/FotoID/${nombreCarpeta}/${archivo.id_archivo}/${archivo.name}`)
          .put(archivo)
          .then(uploadTaskSnapshot => {
            uploadTaskSnapshot.ref
              .getDownloadURL()
              .then(url => {
                resolve({url: url, ruta: nombreCarpeta+'/'+archivo.id_archivo+'/'+archivo.name })
              })
              .catch(error => reject(error))
          })
          .catch(error => reject(error))
      })
    },

    eliminarArchivoFotoID({}, url) {
      return new Promise((resolve, reject) => {
        var storageRef = storage.child('Uploads/FotoID/' + url)
        storageRef
          .delete()
          .then(resp => {
            resolve(resp)
          })
          .catch(error => {
            reject(error)
          })
      })
    },


    
  } /**Fin actions */

} /**Fin store */

export default store
