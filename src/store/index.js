import Vue from 'vue'
import Vuex from 'vuex'

import app from './module/app'
import user from './module/user'
import registro from './module/registro'
import roles from './module/roles'

import prototipos from './module/prototipos'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loading: false,
  },
  getters: {
    //
  },
  mutations: {
    setLoading (state, payload) {
      state.loading = payload
    }
  },
  actions: {
    modalCargando({commit}, status){
      commit('setLoading', status)
    },
  },
  modules: {
    user,
    app,
    registro,
    roles,
    prototipos
  }
})
