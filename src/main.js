// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import BootstrapVue from 'bootstrap-vue'
import JQuery from 'jquery'
import moment from 'moment'
import VueSlideBar from 'vue-slide-bar'
import VueSweetalert2 from 'vue-sweetalert2'
import VueGoodWizard from 'vue-good-wizard'
// eslint-disable-next-line

import axios from 'axios'
import VueAxios from 'vue-axios'
import Vue2Filters from 'vue2-filters'
import Vuelidate from 'vuelidate'
import TextareaAutosize from 'vue-textarea-autosize'
import i18n from '@/locale'
import config from '@/config'
 


let $ = JQuery

const options = {
  confirmButtonColor: '#41b882',
  cancelButtonColor: '#ff7674'
}

Vue.use(BootstrapVue)
Vue.use(VueSweetalert2, options)
Vue.use(VueGoodWizard)
Vue.use(VueAxios, axios)
Vue.use(Vue2Filters)
Vue.use(Vuelidate)
Vue.use(TextareaAutosize)

Vue.component('vue-slide-bar', VueSlideBar)
Vue.config.productionTip = false

Vue.prototype.$config = config
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  i18n,
  store,
  components: {
    App
  },
  template: '<App/>'
})
