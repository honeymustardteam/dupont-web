export const diasSemana = [
    { value: '0', text: 'Domingo' },
    { value: '1', text: 'Lunes' },
    { value: '2', text: 'Martes' },
    { value: '3', text: 'Miércoles' },
    { value: '4', text: 'Jueves' },
    { value: '5', text: 'Viernes' },
    { value: '6', text: 'Sábado' }
  ]
  