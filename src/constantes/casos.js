export const CASOS_BASE_OBJ = {
  CONSULTA: {
    //id_caso:'',
    tipo: '',
    anio:'',
    id_paciente:'',
    id_propietario:'',
    id_sucursal:'',
    ticket:null,
    no_ticket:0,
    servicio_generico: {
      monedero:{aplica:false,cantidad:0},
      descuento_personalizado:0,
      descuento_personalizado_nombre:'',
      pesos_monedero:0,
      completado:true,
      nombre: '',
      precio: 0,
      responsable: '',
      concepto:'',
      concepto_vencimiento:'',
      hora:'',
      cantidad: 1,
      total: 0,
      subtotal: 0,
      monto: 0,
      servicio: [],
      info: {
        fecha_alta: '',
        fecha_vencimiento: ''
      },
      descuento: {},
      activo: true
    },
    anamnesis: {
      motivo_consulta: '',
      diagnostico_corto: '',
      detalles_anamneticos: ''
    },
    historia_clinica: {
      antecedentes_medicos: '',
      antecedentes_zootecnicos: ''
    },
    examen_fisico_general: {
      deshidratacion: 0,
      peso: 0,
      tempetatura: 0,
      f_cardiaca: '',
      f_respiratoria: 0,
      tllc: '',
      pulso: 1,
      estado_mental: '',
      condicion_corporal: '',
      palmopercusion: '',
      reflejo_deglutorio: '',
      reflejo_tusigeno: '',
      mucosas: '',
      linfonodos: '',
      palpacion_andominal: '',
      campos_pulmonares: '',
      oidos: '',
      orificios_naturales: '',
      auscultacion_cardiaca: '',
      valvulas: '',
      patron_respiratorio: '',
      otros_parametros: ''
    },
    medop: {
      lista_problemas: '',
      lista_maestra: '',
      hipotesis_diagnostico: '',
      plan_diagnostico: ''
    },
    examenes_diagnosticos: {
      examenes_complementarios: '',
      diagnostico_presuntivo: '',
      diagnostico_definitivo: ''
    },
    tratamientos_recomendaciones: {
      tratamiento: '',
      recomendaciones: ''
    },
    esquema_dermatologico: {
      notas: ''
    },
    receta: {
      receta: ''
    },
    trazosDibujados: {}
  },
  SERVICIOS_GENERICOS: {
    descuento_personalizado:0,
    descuento_personalizado_nombre:'',
    monedero:{aplica:false,cantidad:0},
    pesos_monedero:0,
    completado:true,
    nombre: '',
    precio: 0,
    responsable: '',
    cantidad: 1,
    concepto:'',
    concepto_vencimiento:'',
    hora:'',
    total: 0,
    subtotal: 0,
    monto: 0,
    servicio: '',
    otros_servicios:'',
    info: {
      fecha_alta: '',
      fecha_vencimiento: ''
    },
    servicio_padre: null,
    activo: true
  },
  SERVICIOS_GENERICOS_PRODUCTOS: {
    descuento_personalizado:0,
    descuento_personalizado_nombre:'',
    monedero:{aplica:false,cantidad:0},
    pesos_monedero:0,
    completado:true,
    nombre: '',
    precio: 0,
    responsable: '',
    cantidad: 1,
    cantidad2:1,
    concepto:'',
    concepto_vencimiento:'',
    hora:'',
    total: 0,
    subtotal: 0,
    monto: 0,
    servicio: '',
    otros_servicios:'',
    info: {
      fecha_alta: '',
      fecha_vencimiento: ''
    },
    servicio_padre: null,
    activo: true
  },
  SERVICIOS_GENERICOS_HOSPITALIZACION: {
    descuento_personalizado:0,
    descuento_personalizado_nombre:'',
    monedero:{aplica:false,cantidad:0},
    pesos_monedero:0,
    nombre: '',
    precio: 0,
    jaula:'',
    responsable: '',
    cantidad: 1,
    concepto:'',
    concepto_vencimiento:'',
    hora:'',
    total: 0,
    subtotal: 0,
    monto: 0,
    servicio: '',
    otros_servicios:'',
    info: {
      fecha_alta: '',
      fecha_vencimiento: ''
    },
    info_entrada:{
      fecha:'',
      hora:'',
    },
    info_salida:{
      fecha:'',
      hora:'',
    },
    completado:false,
    servicio_padre: null,
    activo: true
  },
  HOSPITALIZACION:{
    anio:'',
    id_paciente:'',
    id_propietario:'',
    id_sucursal:'',
    ticket:null,
    no_ticket:0,
    tipo:'',
    servicio_generico:{
      descuento_personalizado:0,
      descuento_personalizado_nombre:'',
      monedero:{aplica:false,cantidad:0},
      pesos_monedero:0,
      completado:false,
      nombre: '',
      precio: 0,
      jaula:'',
      responsable: '',
      cantidad: 1,
      concepto:'',
      concepto_vencimiento:'',
      hora:'',
      total: 0,
      subtotal: 0,
      monto: 0,
      servicio: '',
      otros_servicios:'',
      //TODO:Si llega a fallar borrar el objeto descuento
      descuento:{
        descuentoPorcentaje:'',
        descuentoPrecio:'',
      },
      info: {
        fecha_alta: '',
        fecha_vencimiento: ''
      },
      info_entrada:{
        fecha:'',
        hora:'',
      },
      info_salida:{
        fecha:'',
        hora:'',
      },
      servicio_padre: null,
      activo: true
    },
    servicio_padre:null,
    
  },
  PRODUCTOS: {
    anio:'',
    id_paciente:'',
    id_propietario:'',
    id_sucursal:'',
    ticket:null,
    no_ticket:0,
    tipo: '',
    servicio_generico: {
      descuento_personalizado:0,
      descuento_personalizado_nombre:'',
      monedero:{aplica:false,cantidad:0},
      pesos_monedero:0,
      completado:true,
      nombre: '',
      precio: 0,
      concepto:'',
      concepto_vencimiento:'',
      hora:'',
      responsable: '',
      cantidad: 1,
      cantidad2:1,
      total: 0,
      subtotal: 0,
      monto: 0,
      servicio: [],
      info: {
        fecha_alta: '',
        fecha_vencimiento: ''
      },
      descuento:{
        descuentoPorcentaje:'',
        descuentoPrecio:'',
      },
      activo: true
    },
    servicio_padre: null,
    informacion: {
      concepto: '',
      tipo_servicio:'',
      presentacion: '',
      unidadesMedida: '',
      racion: '',
      dias: 0,
      observaciones: ''
    }
  },
  PRODUCTOS_APLICACION: {
    anio:'',
    id_paciente:'',
    id_propietario:'',
    id_sucursal:'',
    ticket:null,
    no_ticket:0,
    tipo: '',
    servicio_generico: {
      descuento_personalizado:0,
      descuento_personalizado_nombre:'',
      monedero:{aplica:false,cantidad:0},
      pesos_monedero:0,
      completado:true,
      nombre: '',
      precio: 0,
      concepto:'',
      concepto_vencimiento:'',
      hora:'',
      responsable: '',
      cantidad: 1,
      cantidad2:1,
      total: 0,
      subtotal: 0,
      monto: 0,
      servicio: [],
      info: {
        fecha_alta: '',
        fecha_vencimiento: ''
      },
      activo: true
    },
    servicio_padre: null,
    informacion: {
      concepto: '',
      tipo_servicio:'',
      presentacion: '',
      unidadesMedida: '',
      racion: '',
      dias: 0,
      observaciones: '',
      aplicacion:{
        hora_inicio:'',
        aplica_cada:1,
        durante:1,
        periodo:'Día(s)',
        items:[],
        tratamientos:[],
      },
    }
  },
  VACUNACION: {
    anio:'',
    id_paciente:'',
    id_propietario:'',
    id_sucursal:'',
    ticket:null,
    no_ticket:0,
    tipo: '',
    servicio_generico: {
      descuento_personalizado:0,
      descuento_personalizado_nombre:'',
      monedero:{aplica:false,cantidad:0},
      pesos_monedero:0,
      completado:true,
      nombre: '',
      precio: 0,
      concepto:'',
      concepto_vencimiento:'',
      hora:'',
      responsable: '',
      cantidad: 1,
      total: 0,
      subtotal: 0,
      monto: 0,
      servicio: [],
      info: {
        fecha_alta: '',
        fecha_vencimiento: ''
      },
      activo: true
    },
    servicio_padre: null,
    vacunacion: {
      concepto: '',
      marca: '',
      lote: '',
      deshidratacion: 1,
      peso: 0,
      temperatura: 0,
      fCardiaca: 0,
      fRespiratoria: 0,
      tllc: '',
      pulso: 0,
      observaciones: ''
    }
  },
  SOIP: {
    anio:'',
    id_paciente:'',
    id_propietario:'',
    id_sucursal:'',
    ticket:null,
    no_ticket:0,
    tipo: '',
    servicio_generico: {
      descuento_personalizado:0,
      descuento_personalizado_nombre:'',
      monedero:{aplica:false,cantidad:0},
      pesos_monedero:0,
      completado:true,
      nombre: '',
      precio: 0,
      concepto:'',
      concepto_vencimiento:'',
      hora:'',
      responsable: '',
      cantidad: 1,
      total: 0,
      subtotal: 0,
      monto: 0,
      servicio: [],
      info: {
        fecha_alta: '',
        fecha_vencimiento: ''
      },
      activo: true
    },
    servicio_padre: null,
    soip: {
      subjetivo: {
        motivoConsulta: '',
        datosSubjetivos: ''
      },
      objetivo: {
        deshidratacion: 0,
        peso: 0,
        temperatura: 0,
        fCardiaca: 0,
        fRespiratoria: 0,
        tllc: '',
        pulso: 0,
        datosObjetivos: ''
      },
      interpretacion: {
        interpretacion: ''
      },
      plan: {
        plan: ''
      },
      receta: {
        receta: ''
      }
    }
  },
  OTROS: {
    anio:'',
    id_paciente:'',
    id_propietario:'',
    id_sucursal:'',
    ticket:null,
    no_ticket:0,
    tipo: '',
    servicio_generico: {
      descuento_personalizado:0,
      descuento_personalizado_nombre:'',
      monedero:{aplica:false,cantidad:0},
      pesos_monedero:0,
      completado:true,
      nombre: '',
      precio: 0,
      concepto:'',
      concepto_vencimiento:'',
      hora:'',
      responsable: '',
      cantidad: 1,
      total: 0,
      subtotal: 0,
      monto: 0,
      servicio: [],
      otros_servicios:'',
      info: {
        fecha_alta: '',
        fecha_vencimiento: ''
      },
      activo: true
    },
    servicio_padre: null,
    otros: {
      concepto: '',
      jaula: '',
      observaciones: ''
    }
  },
  ESTETICA: {
    anio:'',
    id_paciente:'',
    id_propietario:'',
    id_sucursal:'',
    ticket:null,
    no_ticket:0,
    tipo: '',
    servicio_generico: {
      descuento_personalizado:0,
      descuento_personalizado_nombre:'',
      monedero:{aplica:false,cantidad:0},
      pesos_monedero:0,
      completado:true,
      nombre: '',
      precio: 0,
      concepto:'',
      concepto_vencimiento:'',
      hora:'',
      responsable: '',
      cantidad: 1,
      total: 0,
      subtotal: 0,
      monto: 0,
      servicio: [],
      info: {
        fecha_alta: '',
        fecha_vencimiento: ''
      },
      activo: true
    },
    servicio_padre: null,
    estetica: {
      concepto: '',
      jaula: '',
      peso: '',
      hora_inicio: '',
      hora_fin: '',
      minutos: '',
      hora_entrega: '',
      observaciones: ''
    }
  },
  PLACAS: {
    anio:'',
    id_paciente:'',
    id_propietario:'',
    id_sucursal:'',
    ticket:null,
    no_ticket:0,
    tipo: '',
    servicio_generico: {
      descuento_personalizado:0,
      descuento_personalizado_nombre:'',
      monedero:{aplica:false,cantidad:0},
      pesos_monedero:0,
      completado:true,
      nombre: '',
      precio: 0,
      concepto:'',
      concepto_vencimiento:'',
      hora:'',
      responsable: '',
      cantidad: 1,
      total: 0,
      subtotal: 0,
      monto: 0,
      servicio: [],
      info: {
        fecha_alta: '',
        fecha_vencimiento: ''
      },
      activo: true
    },
    servicio_padre: null,
    placas: {
      concepto: '',
      placa: '',
      propietario: '',
      mascota: '',
      domicilio: '',
      telefono: '',
      material: '',
      color: '',
      observaciones: '',
      status: ''
    }
  },
  ESTUDIOS: {
    anio:'',
    id_paciente:'',
    id_propietario:'',
    id_sucursal:'',
    ticket:null,
    no_ticket:0,
    tipo: '',
    servicio_generico: {
      descuento_personalizado:0,
      descuento_personalizado_nombre:'',
      monedero:{aplica:false,cantidad:0},
      pesos_monedero:0,
      completado:true,
      nombre: '',
      precio: 0,
      concepto:'',
      concepto_vencimiento:'',
      hora:'',
      responsable: '',
      cantidad: 1,
      total: 0,
      subtotal: 0,
      monto: 0,
      servicio: {
        items:[],
      },
      info: {
        fecha_alta: '',
        fecha_vencimiento: ''
      },
      activo: true
    },
    servicio_padre: null,
    laboratorio: {
      laboratorio: {
        concepto: '',
        proveedor: '',
        tipo_proveedor: '',
        observaciones: ''
      },
      analitos: {}
    }
  },
  LABORATORIO: {
    anio:'',
    id_paciente:'',
    id_propietario:'',
    id_sucursal:'',
    ticket:null,
    no_ticket:0,
    tipo: '',
    servicio_generico: {
      descuento_personalizado:0,
      descuento_personalizado_nombre:'',
      monedero:{aplica:false,cantidad:0},
      pesos_monedero:0,
      completado:true,
      nombre: '',
      precio: 0,
      concepto:'',
      concepto_vencimiento:'',
      hora:'',
      responsable: '',
      cantidad: 1,
      total: 0,
      subtotal: 0,
      monto: 0,
      servicio: {
        items:[],
      },
      info: {
        fecha_alta: '',
        fecha_vencimiento: ''
      },
      activo: true
    },
    laboratorio: {
      laboratorio: {
        concepto: '',
        proveedor: '',
        tipo_proveedor: '',
        observaciones: ''
      },
    },
    servicio_padre: null,
  }
}

export const TIPO_SERVICIO_CASO = {
  CONSULTA: 'Consulta',
  VACUNACION: 'Vacunación',
  HOSPITALIZACION:'Hospitalización',
  PRODUCTOS: 'Productos',
  DESPARACITACION: 'Desparasitación',
  SOIP: 'SOIP',
  OTROS: 'Otros',
  IMAGENOLOGIA: 'Imagenología',
  CIRUGIA: 'Cirugía',
  ESTETICA: 'Estética',
  PLACAS: 'Placas',
  LABORATORIO: 'Laboratorio',
  HOTEL:'Hotel',
  ESTUDIOS:'Estudios',
}
