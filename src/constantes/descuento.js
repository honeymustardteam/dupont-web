export const DESCUENTO_BASE_OBJ = {
    tipo_descuento:'pesos',
    id:'',
    nombre:'',
    cantidad:0,
    anio:'',
    aplica_para:'Producto',
    aplica_monedero:false,
    info:{vigencia:'',fecha_alta:'',activo:true},
    items:{
        categoria:[],
        subcategoria:[],
        linea:[],
        elementos:[],
    },
    mostrar:{
        muestra_subcategoria:false,
        muestra_linea:false,
        muestra_elementos:false,
    }
}

export const DESCUENTO_SERVICIO_BASE_OBJ = {
    tipo_descuento:'pesos',
    nombre:'',
    cantidad:0,
    anio:'',
    aplica_para:'Servicio',
    aplica_monedero:false,
    info:{vigencia:'',fecha_alta:'',activo:true},
    items:{
        categoria:[],
        subcategoria:[],
        linea:[],
        elementos:[],
    },
    mostrar:{
        muestra_subcategoria:false,
        muestra_linea:false,
        muestra_elementos:false,
    }
}

export const MUESTRA_DESCUENTO_BASE_OBJ = {
    muestra_subcategoria:false,
    muestra_linea:false,
    muestra_todos:false,
}