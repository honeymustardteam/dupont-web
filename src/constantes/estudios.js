export const ESTUDIOS_BASE_OBJ = {
    ESTUDIOS:{
        children: [
            { // EJEMPLO
                children: [
                    {},
                    {
                        id: 'MmKoqqwyf7pekRa4hCz2',
                        delete: false,
                        name: 'estudio',
                        name_nivel1:'Especie',
                        id_nivel1:'a164316b-1b2b-4fd7-b94a',
                        nivel: 2
                    }
                ],
                delete: false,
                id: 'a164316b-1b2b-4fd7-b94a',
                name: 'Especie',
                nivel: 1
            },
           
        ],
        name: "Estudios"
    },
    ESTUDIO:{
        items: [
            {
                invervalos: {max: "4", min: "3"},
                nombre: "nombre",
                unidad: "g/L"
            }
        ],
        delete: false,
        especie: "Canino",
        nombre: "Estudio",
        precio: "00",
        compartir:{
            es_publico:true,
            en_sucursales:{},
        },
        estatus:{
            fecha_baja: {}, 
            activo: 'activo',
        },
        servicio_padre:"",
    }
  }