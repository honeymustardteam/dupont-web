export const ROL_BASE_OBJ = {
  info: { nombre: '' },
  modulos: { 
    imagenes: {
      accesos: false, 
      informacion: {
        consultar: false,
        crear: false,
        editar: false,
        eliminar: false,
        asig_usuario: false,
        docs: false,
        contratos: false,
        afianza: false,
        inactivos: false,
      }
    },

    proveedores: {
      accesos: false, 
      informacion: {
        consultar: false,
        crear: false,
        editar: false,
        eliminar: false,
        asig_usuario: false,
        docs: false,
        contratos: false,
        afianza: false,
        inactivos: false,
      }
    },

    expedienteadmin: {
      accesos: false, 
      informacion: {
        consultar: false,
        crear: false,
        editar: false,
        eliminar: false,
        asig_usuario: false,
        docs: false,
        contratos: false,
        afianza: false,
        inactivos: false,
      }
    },

    contratos: {
      accesos: false, 
      informacion: {
        consultar: false,
        crear: false,
        editar: false,
        eliminar: false,
        asig_usuario: false,
        docs: false,
        contratos: false,
        afianza: false,
        inactivos: false,
      }
    },

    afianza: {
      accesos: false, 
      informacion: {
        consultar: false,
        crear: false,
        editar: false,
        eliminar: false,
        asig_usuario: false,
        docs: false,
        contratos: false,
        afianza: false,
        inactivos: false,
      }
    },

    usuarios: {
      accesos: false, 
      informacion: {
        consultar: false,
        crear: false,
        editar: false,
        eliminar: false,
        asig_usuario: false,
        docs: false,
        contratos: false,
        afianza: false,
        inactivos: false,
      }
    },

    requisitos: {
      accesos: false, 
      informacion: {
        consultar: false,
        crear: false,
        editar: false,
        eliminar: false,
        asig_usuario: false,
        docs: false,
        contratos: false,
        afianza: false,
        inactivos: false,
      }
    },

    contacto: {
      accesos: false, 
      informacion: {
        consultar: false,
        crear: false,
        editar: false,
        eliminar: false,
        asig_usuario: false,
        docs: false,
        contratos: false,
        afianza: false,
        inactivos: false,
      }
    },

    informacion: {
      accesos: false, 
      informacion: {
        consultar: false,
        crear: false,
        editar: false,
        eliminar: false,
        asig_usuario: false,
        docs: false,
        contratos: false,
        afianza: false,
        inactivos: false,
      }
    },

    tickets: {
      accesos: false, 
      informacion: {
        consultar: false,
        crear: false,
        editar: false,
        eliminar: false,
        asig_usuario: false,
        docs: false,
        contratos: false,
        afianza: false,
        inactivos: false,
      }
    },

    notificaciones: {
      accesos: false, 
      informacion: {
        consultar: false,
        crear: false,
        editar: false,
        eliminar: false,
        asig_usuario: false,
        docs: false,
        contratos: false,
        afianza: false,
        inactivos: false,
      }
    },

    todos_proveedores: {
      accesos: false, 
      informacion: {
        consultar: false,
        crear: false,
        editar: false,
        eliminar: false,
        asig_usuario: false,
        docs: false,
        contratos: false,
        afianza: false,
        inactivos: false,
      }
    },


  }
}
