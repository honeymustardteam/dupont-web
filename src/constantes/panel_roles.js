export const ROL_BASE_OBJ = {
  info: { nombre: '' },
  modulos: { 
    clientes: {
      accesos: false, 
      informacion: {
        consultar: false,
        crear: false,
        editar: false,
        eliminar: false
      }
    },
    sucursales: {
      accesos: false, 
      informacion: {
        consultar: false,
        crear: false,
        editar: false,
        eliminar: false
      }
    },
    empleados: {
      accesos: false, 
      informacion: {
        consultar: false,
        crear: false,
        editar: false,
        eliminar: false
      }
    },
    roles: {
      accesos: false, 
      informacion: {
        consultar: false,
        crear: false,
        editar: false,
        eliminar: false
      }
    },
  }
}
