export const PACIENTE_BASE_OBJ = {
  info: { fecha_alta: {}, fecha_nacimiento: null, sexo: '' },
  habitos: { frecuencia_dieta: '' },
  info_complementaria: {peso:0,},
  estatus: { fecha_baja: {} },
  propietarios: [],
  personas_autorizados: [],
  especie: {
    display_nombre_comun: '',
    display_nombre_raza: '',
    nombre_especie: '',
    nombre_cientifico: '',
    nombre_comun: '',
    nombre_raza: ''
  },
  foto: { nombre: '', url: '' },
  color: []
}
