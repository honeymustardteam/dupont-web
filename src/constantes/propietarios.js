export const PROPIETARIO_BASE_OBJ = {
  id: '',
  info: { fecha_alta: {}, fecha_nacimiento: {},sexo:'',nivel_apego:'' },
  datos_contacto: {twitter:'',contactado_twitter:false,correo:'',contactado_correo1:false},
  direccion: {},
  datos_fiscales: { estado:'' },
  estatus: { fecha_baja: {} },
  fecha_alta: {},
  pacientes: [],
  arrPacientes: {},
  casos:[],
  monedero:0,
  anticipos:[],
}
