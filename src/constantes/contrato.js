export const CONTRATO_BASE_OBJ = {
  nombre: '',
  id_proveedor: '',
  fecha_inicio: '',
  fecha_final: '',
  status: 'Vigente',
  monto: '',
  pdf: false,
  afianzadora: null,
  monto_afianzadora: '',
  activo: true,

  esquema: null,
  numero: '',
  firma: '',
  relativo: '',
}
