export const REGISTRO_BASE_OBJ = {
  nombre_completo: '',
  nombre_empresa: '',
  funcion: '',
  correo: '',
  contrasenia: '',
  repetirPass: '',
  
  access: ['registrado'],
  es_registrado: true,
  es_responsable: false,
  activo: true,
}
