import { required, sameAs, minLength } from 'vuelidate/lib/validators'

export const REGISTRO_REQUERIDOS_OBJ = {
  nombre_completo: {
    required
  },
  /*nombre_empresa: {
    required
  },
  funcion: {
    required
  },*/
  correo: {
    required
  },
  /*
  contrasenia: {
    required,
    minLength: minLength(6)
  },
  repetirPass: {
    sameAsPassword: sameAs('contrasenia')
  }
  */
}
