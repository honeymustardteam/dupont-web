export const TICKET_BASE_OBJ = {
    forma_pago:'',
    id_corte:'',
    fecha:'',
    referencia:'',
    banco:'',
    terminal:'',
    monto:0,
  };

  export const TICKET_GENERADO_BASE_OBJ = {
    responsable:null,
    numero_ticket:'',
    id_propietario:'',
    id_numerico:0,
    sucursal:'',
    anio:'',
    pagos_pendientes:[],
    pagos_realizados:[],
    finalizado:false,
    saldo:'',
    fecha:'',
    total_pagos_pendientes:0,
    total_pagos_realizados:0,
  };