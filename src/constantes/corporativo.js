export const CORPORATIVO_BASE_OBJ = {
  direccion: {}
}

export const SUCURSAL_BASE_OBJ = {
  info: {},
  direccion: {pais: '', estado:''},
  datos_fiscales: { estado: '', pais: '' },
  estatus: { fecha_baja: {} }
}
