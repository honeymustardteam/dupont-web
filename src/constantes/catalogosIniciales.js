export const ESPACIOS = {
  Espacios: [{ activo: true, nombre: 'Consultorio 1', sucursal: {} }]
}

export const TICKET_ID = {
  id_ticket_count: 1,
}

export const CAJAS = {
  Cajas: [{ activo: true, id_temporal:0, nombre: 'Caja 1', sucursal: {} }]
}
/*export const COLORES = {
  Colores: [
    {
      nombre: 'Azul'
    },
    {
      nombre: 'Amarillo'
    },
    {
      nombre: 'Blanco'
    },
    {
      nombre: 'Verde'
    },
    {
      nombre: 'Café'
    },
    {
      nombre: 'Manchado'
    },
    {
      nombre: 'Rayado'
    }
  ]
}
*/ 
export const COLORES = {
  Colores: [
    {
      id_temporal: 0,
      activo: true,
      nombre: 'Negro'
    },
    {
      id_temporal: 1,
      activo: true,
      nombre: 'Blanco'
    },
    {
      id_temporal: 2,
      activo: true,
      nombre: 'Azul'
    },
    {
      id_temporal: 3,
      activo: true,
      nombre: 'Rojo'
    },
    {
      id_temporal: 4,
      activo: true,
      nombre: 'Amarillo'
    },
    {
      id_temporal: 5,
      activo: true,
      nombre: 'Verde'
    },
    {
      id_temporal: 6,
      activo: true,
      nombre: 'Morado'
    },
    {
      id_temporal: 7,
      activo: true,
      nombre: 'Rayado'
    },
    {
      id_temporal: 8,
      activo: true,
      nombre: 'Atigrado'
    },
    {
      id_temporal: 9,
      activo: true,
      nombre: 'Pinto'
    },
    {
      id_temporal: 10,
      activo: true,
      nombre: 'Café'
    },
    {
      id_temporal: 11,
      activo: true,
      nombre: 'Beigue'
    },
    {
      id_temporal: 12,
      activo: true,
      nombre: 'Gris'
    },
    {
      id_temporal: 13,
      activo: true,
      nombre: 'Turquesa'
    },
    {
      id_temporal: 14,
      activo: true,
      nombre: 'Moteado'
    },
    {
      id_temporal: 15,
      activo: true,
      nombre: 'Naranja'
    },
  ]
}