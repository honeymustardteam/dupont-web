export const PRODUCTOS_BASE_OBJ = {
    PRODUCTOS:{
        children: [
            { // PRODOCTO EJEMPLO
                children: [ //Categoria
                    {},
                    {
                        children: [ //Subcategoria
                            {},
                            {
                                children: [ //Linea
                                    {},
                                    { //Producto
                                        delete: false,
                                        id: 'OvrJ0058hSeji0Xe8dTo',
                                        id_nivel1:'b47348d5-48c0-4801-8d12',
                                        id_nivel2:'b26dab22-adf2-4588-b5fc',
                                        id_nivel3:'ee2b1c38-1ff6-4723-8b74',
                                        name: 'Producto1',
                                        name_nivel1:'Categoria',
                                        name_nivel2:'Subcategoria',
                                        name_nivel3:'Linea',
                                        nivel: 4 
                                    }
                                    
                                ],
                                delete: false,
                                id: 'ee2b1c38-1ff6-4723-8b74',
                                id_nivel1:'b47348d5-48c0-4801-8d12',
                                id_nivel2:'b26dab22-adf2-4588-b5fc',
                                name: 'Linea',
                                name_nivel1:'Categoria',
                                name_nivel2:'Subcategoria',
                                nivel: 3
                            }
                            
                        ],
                        id: 'b26dab22-adf2-4588-b5fc',
                        id_nivel1:'b47348d5-48c0-4801-8d12',
                        delete: false,
                        name: 'Subcategoria',
                        name_nivel1:'Categoria',
                        nivel: 2
                    }
                ],
                delete: false,
                id: 'b47348d5-48c0-4801-8d12',
                name: 'Categoria',
                nivel: 1
            },
           
        ],
        name: "Productos"
    },
    PRODUCTO: {
        codigoBarra:'',nombre:'Producto1',sucursal:[],claveSat:'',descripcion:'',
        presentacion:0,unidadMedida:'Gramos (g)',marca:'',precioCompra:100.00,SKU:'SKU',precioVenta:100.00,
        codigoBarraFabrica:'9955437465476',estatus:{
          fecha_baja: {},
          activo: 'activo',
        },info:{
          fecha_alta:'',
          fecha_nacimiento:'',
        }, motivosBaja:'',
        compartir:{es_publico:true,en_sucursales:{}}
    }
  }