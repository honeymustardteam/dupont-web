export const meses = [
    { value: '01', text: 'enero' },
    { value: '02', text: 'febrero' },
    { value: '03', text: 'marzo' },
    { value: '04', text: 'abril' },
    { value: '05', text: 'mayo' },
    { value: '06', text: 'junio' },
    { value: '07', text: 'julio' },
    { value: '08', text: 'agosto' },
    { value: '09', text: 'septiembre' },
    { value: '10', text: 'octubre' },
    { value: '11', text: 'noviembre' },
    { value: '12', text: 'diciembre' }
  ]