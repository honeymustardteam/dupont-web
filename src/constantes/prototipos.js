export const PROTOTIPO_BASE_OBJ = {
    PROTOTIPO: {
        user_id: null,
        responsable: false,
        completado: false,
        especificaciones:{
            completado: false,
            pasos_completados: {
              paso1: false,
              paso2: false,
              paso3: false,
              paso4: false,
              paso5: false,
              paso6: false,
              paso7: false,
              paso8: false,
            },
  
            //PASO 1
            numero_prototipo: null,
            nombre_proyecto: null,
            plataforma: null,
            modelo: null,
            pzasTotales: null,
            fecha: '',
  
            //PASO 2
            tipoClase: '',
  
            //PASO 3
            ubicacion_pieza: null,
  
            //PASO 4
            pieza_interior: '',
            pieza_exterior: '',
  
            //PASO 5
            forma_pieza: '',
  
            //paso 6
            altura_pieza: 1,
            ancho_pieza: 1,
            largo_pieza: 1,
  
            //paso 7 peso
            peso_num: 0,
            es_kg: true,
  
            //paso 8 acabado
            acabado: 'Graining',
  
        },
        tipo_contenedor:{
            completado: false,
            tipo_contenedor: null,
            otro:{
              altura: null,
              ancho: null,
              largo: null
            }
        },
        num_piezas:{
            completado: false,
            num_piezas: 1,
            num_piezas: false,
        },
        tipo_uso:{
          tipo_uso: null,
          completado: false,
        },
        almacenamiento:{
          almacenamiento: null,
          completado: false,
        },
        dunnage:{
            completado: false,
            dunnageR: null,
            colorM: '',
            acabado: '',
            nivel4: '',
            nivel3: '',
            nivel2: '',
            nivel1: '',
        },
        solicitar_btn:{
          disenio: false,
          prototipo: false,
        },
        solicitar_disenio: false,
        solicitar: false,
        estatus: 'Pendiente',
        activo: true,
      },
  }