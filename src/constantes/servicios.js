export const SERVICIO_NORMAL_BASE_OBJ = {
  codigoBarra:'',nombre:'',sucursal:[],claveSat:'',descripcion:'',
  unidadMedida:'',precioCompra:0,SKU:'',precioVenta:0, otros_servicios:false,
  estatus:{ fecha_baja: {}, activo: 'activo', }
  ,info:{ fecha_alta:'', fecha_nacimiento:'', },
  motivosBaja:'', compartir:{es_publico:true,en_sucursales:{}}
};

export const SERVICIO_HOSPITALIZACION_BASE_OBJ = {
  codigoBarra:'',nombre:'',sucursal:[],claveSat:'',descripcion:'',
  unidadMedida:'',precioCompra:0,SKU:'',precioVenta:0, otros_servicios:false,
  estatus:{ fecha_baja: {}, activo: 'activo', }
  ,info:{ fecha_alta:'', fecha_nacimiento:'', },
  motivosBaja:'', compartir:{es_publico:true,en_sucursales:{}},
  extras:{
    es_hotel:true,es_proporcional:false,cantidad:1,periodo:'Día'
  }
}

/*************************  NUEVA IMPLEMENTACION  */
export const SERVICIOS_BASE_OBJ = {
  children: [
    {
      children: [
        {},
        {
          delete: false,
          id: '00f62c77-c106-4e5f-ae52',
          id_nivel1:'b4734zd5-48c0-4801-8d13',
          name: 'Consulta General',
          name_nivel1:'Consulta',
          nivel: 2,
          children: [
            {},
            {
              delete: false,
              id: 'pmOawkImsDTecuZJaiZ9',
              id_nivel1:'b4734zd5-48c0-4801-8d13',
              id_nivel2:'00f62c77-c106-4e5f-ae52',
              name: 'Consulta General',
              name_nivel1:'Consulta',
              name_nivel2:'Consulta General',
              nivel: 3
            },
            {
              delete: false,
              id: 'b3c0W6wjv2jM09ymvPeN',
              id_nivel1:'b4734zd5-48c0-4801-8d13',
              id_nivel2:'00f62c77-c106-4e5f-ae52',
              name: 'Consulta de Seguimiento',
              name_nivel1:'Consulta',
              name_nivel2:'Consulta General',
              nivel: 3
            },
            {
              delete: false,
              id: 'W2TjK8My3xXv8Gw2pdii',
              id_nivel1:'b4734zd5-48c0-4801-8d13',
              id_nivel2:'00f62c77-c106-4e5f-ae52',
              name: 'Consulta a Domicilio',
              name_nivel1:'Consulta',
              name_nivel2:'Consulta General',
              nivel: 3
            }
          ]
        }
      ],
      delete: false,
      id: 'b4734zd5-48c0-4801-8d13',
      name: 'Consulta',
      nivel: 1,
      no_eliminar: true
    },
    //Nuevo servicio
    {
      children: [
        {},
        {
          children: [
            {},
            {
              delete: false,
              id: 'ItZAfeb7x9XKlEzNWE0U',
              id_nivel1:'b4734zd5-48c0-4801-8d15',
              id_nivel2:'f0926de9-b2a2-4d95-8f4d',
              name: 'Rabia',
              name_nivel1:'Vacunación',
              name_nivel2:'Vacunación',
              nivel: 3
            }
          ],
          delete: false,
          id: 'f0926de9-b2a2-4d95-8f4d',
          id_nivel1:'b4734zd5-48c0-4801-8d15',
          name: 'Vacunación',
          name_nivel1:'Vacunación',
          nivel: 2
        }
      ],
      delete: false,
      id: 'b4734zd5-48c0-4801-8d15',
      name: 'Vacunación',
      nivel: 1,
      no_eliminar: true
    },
    //Nuevo servicio
    {
      children: [
        {},
        {
          children: [
            {},
            {
              delete: false,
              id: 'E3tRsRBZHQ1N1KCXXn7U',
              id_nivel1:'b4734zd5-48c0-4801-8d16',
              id_nivel2:'a4d3f676-c0f6-4a35-aa40',
              name: 'Desparasitación',
              name_nivel1:'Desparasitación',
              name_nivel2:'Desparasitación',
              nivel: 3
            }
          ],
          delete: false,
          id: 'a4d3f676-c0f6-4a35-aa40',
          id_nivel1:'b4734zd5-48c0-4801-8d16',
          name: 'Desparasitación',
          name_nivel1:'Desparasitación',
          nivel: 2
        }
      ],
      delete: false,
      id: 'b4734zd5-48c0-4801-8d16',
      name: 'Desparasitación',
      nivel: 1,
      no_eliminar: true
    },
    //Nuevo servicio
    {
      children: [
        {},
        {
          children: [
            {},
            {
              delete: false,
              id: 'R0k9A9kRCsqH1HUCBOlf',
              id_nivel1:'b4734zd5-48c0-4801-8d17',
              id_nivel2:'deaa0a2b-ef4c-44e1-bfc9',
              name: 'SOIP',
              name_nivel1:'SOIP',
              name_nivel2:'SOIP',
              nivel: 3
            }
          ],
          delete: false,
          id: 'deaa0a2b-ef4c-44e1-bfc9',
          id_nivel1:'b4734zd5-48c0-4801-8d17',
          name: 'SOIP',
          name_nivel1:'SOIP',
          nivel: 2
        }
      ],
      delete: false,
      id: 'b4734zd5-48c0-4801-8d17',
      name: 'SOIP',
      nivel: 1,
      no_eliminar: true
    },
    //Nuevo servicio
    {
      children: [
        {},
        {
          children: [
            {},
            {
              delete: false,
              id: 'eZfUTUjRiobUTzVhd3zu',
              id_nivel1:'b4734zd5-48c0-4801-8d19',
              id_nivel2:'90c3d47f-f2ef-4698-9e6f',
              name: 'Cirugía',
              name_nivel1:'Cirugía',
              name_nivel2:'Cirugía',
              nivel: 3
            }
          ],
          delete: false,
          id: '90c3d47f-f2ef-4698-9e6f',
          id_nivel1:'b4734zd5-48c0-4801-8d19',
          name: 'Cirugía',
          name_nivel1:'Cirugía',
          nivel: 2
        }
      ],
      delete: false,
      id: 'b4734zd5-48c0-4801-8d19',
      name: 'Cirugía',
      nivel: 1,
      no_eliminar: true
    },
    //Nuevo servicio
    {
      children: [
        {},
        {
          children: [
            {},
            {
              delete: false,
              id: 'unf7aBn2JI2ckBljT60W',
              id_nivel1:'b4734zd5-48c0-4801-8d20',
              id_nivel2:'4baacb2e-77c0-4f44-bae0',
              name: 'Imagenología',
              name_nivel1:'Imagenología',
              name_nivel2:'Imagenología',
              nivel: 3
            }
          ],
          delete: false,
          id: '4baacb2e-77c0-4f44-bae0',
          id_nivel1:'b4734zd5-48c0-4801-8d20',
          name: 'Imagenología',
          name_nivel1:'Imagenología',
          nivel: 2
        }
      ],
      delete: false,
      id: 'b4734zd5-48c0-4801-8d20',
      name: 'Imagenología',
      nivel: 1,
      no_eliminar: true
    },
    //Nuevo servicio
    {
      children: [
        {},
        {
          children: [
            {},
            {
              delete: false,
              id: 'yiuAQYKVYwskrkocK7AJ',
              id_nivel1:'b4734zd5-48c0-4801-8d21',
              id_nivel2:'702035d7-d9e0-4976-90c9',
              name: 'Estética',
              name_nivel1:'Estética',
              name_nivel2:'Estética',
              nivel: 3
            },
            {
              delete: false,
              id: 'D72obyzJlfhYJbQ6yLJy',
              id_nivel1:'b4734zd5-48c0-4801-8d21',
              id_nivel2:'702035d7-d9e0-4976-90c9',
              name: 'Baño',
              name_nivel1:'Estética',
              name_nivel2:'Estética',
              nivel: 3
            }
          ],
          delete: false,
          id: '702035d7-d9e0-4976-90c9',
          id_nivel1:'b4734zd5-48c0-4801-8d21',
          name: 'Estética',
          name_nivel1:'Estética',
          nivel: 2
        }
      ],
      delete: false,
      id: 'b4734zd5-48c0-4801-8d21',
      name: 'Estética',
      nivel: 1,
      no_eliminar: true
    },
    //Nuevo servicio
    {
      children: [
        {},
        {
          children: [
            {},
            { 
              delete: false,
              id: 'HmkLv79JyjDE9SfjPBAS',
              id_nivel1:'b4734zd5-48c0-4801-8d22',
              id_nivel2:'7e4ca9ac-41e2-44b3-9478',
              name: 'RX',
              name_nivel1:'Placas',
              name_nivel2:'Placas',
              nivel: 3 
            }
          ],
          delete: false,
          id: '7e4ca9ac-41e2-44b3-9478',
          id_nivel1:'b4734zd5-48c0-4801-8d22',
          name: 'Placas',
          name_nivel1:'Placas',
          nivel: 2
        }
      ],
      delete: false,
      id: 'b4734zd5-48c0-4801-8d22',
      name: 'Placas',
      nivel: 1,
      no_eliminar: true
    },
    //Nuevo servicio
    {
      children: [
        {},
        {
          children: [
            {},
            {
              delete: false,
              id: 'ucm5XKiKZdeqBFjKfwyy',
              id_nivel1:'b4734zd5-48c0-4801-8d23',
              id_nivel2:'f7aa8f70-5518-46c6-b48c',
              name: 'Prueba Cortisol 3',
              name_nivel1:'Laboratorio',
              name_nivel2:'Laboratorio',
              nivel: 3
            }
          ],
          delete: false,
          id: 'f7aa8f70-5518-46c6-b48c',
          id_nivel1:'b4734zd5-48c0-4801-8d23',
          name: 'Laboratorio',
          name_nivel1:'Laboratorio',
          nivel: 2
        }
      ],
      delete: false,
      id: 'b4734zd5-48c0-4801-8d23',
      name: 'Laboratorio',
      nivel: 1,
      no_eliminar: true
    },
    //Nuvo servicio
    {
      children: [
        {},
        {
          children: [
            {},
            {
              delete: false,
              id: 'F0cobcKZC37JOSVadUpE',
              id_nivel1:'6b3262e6-bacf-4dec-a1c6',
              id_nivel2:'7343bcd6-b2dc-438c-9e50',
              name: 'Hospitalización',
              name_nivel1:'Hospitalización',
              name_nivel2:'Hospitalización',
              es_hotel:true,
              nivel: 3
            }
          ],
          delete: false,
          id: '7343bcd6-b2dc-438c-9e50',
          id_nivel1:'6b3262e6-bacf-4dec-a1c6',
          name_nivel1:'Hospitalización',
          name: 'Hospitalización',
          es_hotel:true,
          nivel: 2
        }
      ],
      delete: false,
      id: '6b3262e6-bacf-4dec-a1c6',
      name: 'Hospitalización',
      nivel: 1,
      no_eliminar: true
    },
    //Nuevo servicio
    {
      children: [
        {},
        {
          children: [
            {},
            {
              delete: false,
              id: 'UK3FetKpMQznSqgHQ1Zi',
              id_nivel1:'bb52a3d6-2adf-4ca2-97cb',
              id_nivel2:'4bb5a55a-f4fb-48e0-8787',
              name_nivel1:'Hotel',
              name_nivel2:'Hotel',
              name: 'Pensión',
              es_hotel:true,
              nivel: 3
            }
          ],
          delete: false,
          id: '4bb5a55a-f4fb-48e0-8787',
          id_nivel1:'bb52a3d6-2adf-4ca2-97cb',
          name: 'Hotel',
          name_nivel1:'Hotel',
          es_hotel:true,
          nivel: 2
        }
      ],
      delete: false,
      id: 'bb52a3d6-2adf-4ca2-97cb',
      name: 'Hotel',
      nivel: 1,
      no_eliminar: true
    }
  ],
  name: 'Servicios'
}

export const SERVICIOS_BASE_INICIAL = {
  pmOawkImsDTecuZJaiZ9: {
    SKU: '000',
    claveSat: '',
    codigoBarra: '9914965597851',
    compartir: {
      en_sucursales: {},
      es_publico: true
    },
    descripcion: '',
    estatus: { activo: 'activo', fecha_baja: {} },
    info: { fecha_alta: '', fecha_nacimiento: '' },
    motivosBaja: '',
    nombre: 'Consulta General',
    precioCompra: 300.00,
    precioVenta: 350.00,
    servicio_padre: 'b4734zd5-48c0-4801-8d13',
    unidadMedida: ''
  },
  b3c0W6wjv2jM09ymvPeN: {
    SKU: '000',
    claveSat: '',
    codigoBarra: '9914965973488',
    compartir: {
      en_sucursales: {},
      es_publico: true
    },
    descripcion: '',
    estatus: { activo: 'activo', fecha_baja: {} },
    info: { fecha_alta: '', fecha_nacimiento: '' },
    motivosBaja: '',
    nombre: 'Consulta de Seguimiento',
    precioCompra: 25.00,
    precioVenta: 50.00,
    servicio_padre: 'b4734zd5-48c0-4801-8d13',
    unidadMedida: ''
  },
  W2TjK8My3xXv8Gw2pdii: {
    SKU: '000',
    claveSat: '',
    codigoBarra: '9914966141909',
    compartir: {
      en_sucursales: {},
      es_publico: true
    },
    descripcion: '',
    estatus: { activo: 'activo', fecha_baja: {} },
    info: { fecha_alta: '', fecha_nacimiento: '' },
    motivosBaja: '',
    nombre: 'Consulta a Domicilio',
    precioCompra: 400.00,
    precioVenta: 450.00,
    servicio_padre: 'b4734zd5-48c0-4801-8d13',
    unidadMedida: ''
  },
  ItZAfeb7x9XKlEzNWE0U: {
    SKU: '000',
    claveSat: '',
    codigoBarra: '9914966317960',
    compartir: { en_sucursales: {}, es_publico: true },
    descripcion: '',
    estatus: { activo: 'activo', fecha_baja: {} },
    info: { fecha_alta: '', fecha_nacimiento: '' },
    motivosBaja: '',
    nombre: 'Rabia',
    precioCompra: 50.00,
    precioVenta: 100.00,
    servicio_padre: 'b4734zd5-48c0-4801-8d15',
    unidadMedida: ''
  },
  E3tRsRBZHQ1N1KCXXn7U: {
    SKU: '000',
    claveSat: '',
    codigoBarra: '9914966404141',
    compartir: { en_sucursales: {}, es_publico: true },
    descripcion: '',
    estatus: { activo: 'activo', fecha_baja: {} },
    info: { fecha_alta: '', fecha_nacimiento: '' },
    motivosBaja: '',
    nombre: 'Desparasitación',
    precioCompra: 50.00,
    precioVenta: 100.00,
    servicio_padre: 'b4734zd5-48c0-4801-8d16',
    unidadMedida: ''
  },
  R0k9A9kRCsqH1HUCBOlf: {
    SKU: '000',
    claveSat: '',
    codigoBarra: '9914966506395',
    compartir: { en_sucursales: {}, es_publico: true },
    descripcion: '',
    estatus: { activo: 'activo', fecha_baja: {} },
    info: { fecha_alta: '', fecha_nacimiento: '' },
    motivosBaja: '',
    nombre: 'SOIP',
    precioCompra: 100.00,
    precioVenta: 150.00,
    servicio_padre: 'b4734zd5-48c0-4801-8d17',
    unidadMedida: ''
  },
  eZfUTUjRiobUTzVhd3zu: {
    SKU: '000',
    claveSat: '',
    codigoBarra: '9914966572550',
    compartir: { en_sucursales: {}, es_publico: true },
    descripcion: '',
    estatus: { activo: 'activo', fecha_baja: {} },
    info: { fecha_alta: '', fecha_nacimiento: '' },
    motivosBaja: '',
    nombre: 'Cirugía',
    precioCompra: 100.00,
    precioVenta: 150.00,
    servicio_padre: 'b4734zd5-48c0-4801-8d19',
    unidadMedida: ''
  },
  unf7aBn2JI2ckBljT60W: {
    SKU: '000',
    claveSat: '',
    codigoBarra: '9914966711102',
    compartir: { en_sucursales: {}, es_publico: true },
    descripcion: '',
    estatus: { activo: 'activo', fecha_baja: {} },
    info: { fecha_alta: '', fecha_nacimiento: '' },
    motivosBaja: '',
    nombre: 'Imagenología',
    precioCompra: 30.00,
    precioVenta: 50.00,
    servicio_padre: 'b4734zd5-48c0-4801-8d20',
    unidadMedida: ''
  },
  yiuAQYKVYwskrkocK7AJ: {
    SKU: '000',
    claveSat: '',
    codigoBarra: '9914966770079',
    compartir: { en_sucursales: {}, es_publico: true },
    descripcion: '',
    estatus: { activo: 'activo', fecha_baja: {} },
    info: { fecha_alta: '', fecha_nacimiento: '' },
    motivosBaja: '',
    nombre: 'Estética',
    precioCompra: 100.00,
    precioVenta: 150.00,
    servicio_padre: 'b4734zd5-48c0-4801-8d21',
    unidadMedida: ''
  },
  D72obyzJlfhYJbQ6yLJy: {
    SKU: '000',
    claveSat: '',
    codigoBarra: '9914966949772',
    compartir: { en_sucursales: {}, es_publico: true },
    descripcion: '',
    estatus: { activo: 'activo', fecha_baja: {} },
    info: { fecha_alta: '', fecha_nacimiento: '' },
    motivosBaja: '',
    nombre: 'Baño',
    precioCompra: 100.00,
    precioVenta: 100.00,
    servicio_padre: 'b4734zd5-48c0-4801-8d21',
    unidadMedida: ''
  },
  HmkLv79JyjDE9SfjPBAS: {
    SKU: '000',
    claveSat: '',
    codigoBarra: '9914967031315',
    compartir: { en_sucursales: {}, es_publico: true },
    descripcion: '',
    estatus: { activo: 'activo', fecha_baja: {} },
    info: { fecha_alta: '', fecha_nacimiento: '' },
    motivosBaja: '',
    nombre: 'RX',
    precioCompra: 50.00,
    precioVenta: 75.00,
    servicio_padre: 'b4734zd5-48c0-4801-8d22',
    unidadMedida: ''
  },
  ucm5XKiKZdeqBFjKfwyy: {
    SKU: '000',
    claveSat: '',
    codigoBarra: '9914967111291',
    compartir: { en_sucursales: {}, es_publico: true },
    descripcion: '',
    estatus: { activo: 'activo', fecha_baja: {} },
    info: { fecha_alta: '', fecha_nacimiento: '' },
    motivosBaja: '',
    nombre: 'Prueba Cortisol 3',
    precioCompra: 100.00,
    precioVenta: 150.00,
    servicio_padre: 'b4734zd5-48c0-4801-8d23',
    unidadMedida: ''
  },
  F0cobcKZC37JOSVadUpE: {
    SKU: '000',
    claveSat: '',
    codigoBarra: '9914967189283',
    compartir: { en_sucursales: {}, es_publico: true },
    descripcion: '',
    estatus: { activo: 'activo', fecha_baja: {} },
    info: { fecha_alta: '', fecha_nacimiento: '' },
    motivosBaja: '',
    nombre: 'Hospitalización',
    precioCompra: 0.00,
    precioVenta: 100.00,
    servicio_padre: '6b3262e6-bacf-4dec-a1c6',
    unidadMedida: '',
    extras:{
      es_hotel:true,es_proporcional:false,cantidad:1,periodo:'Día'
    }
  },
  UK3FetKpMQznSqgHQ1Zi: {
    SKU: '000',
    claveSat: '',
    codigoBarra: '9914967268490',
    compartir: { en_sucursales: {}, es_publico: true },
    descripcion: '',
    estatus: { activo: 'activo', fecha_baja: {} },
    info: { fecha_alta: '', fecha_nacimiento: '' },
    motivosBaja: '',
    nombre: 'Pensión',
    precioCompra: 0.00,
    precioVenta: 100.00,
    servicio_padre: 'bb52a3d6-2adf-4ca2-97cb',
    unidadMedida: '',
    extras:{
      es_hotel:true,es_proporcional:false,cantidad:1,periodo:'Día'
    }
  }
}
