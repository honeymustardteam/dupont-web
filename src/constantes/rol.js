export const ROLES_BASE_OBJ = {
    ROL: {
        info: { nombre: 'Administrador' },
        modulos: { 
            propietarios: {
            accesos: true, 
            informacion: {
                consultar: true,
                crear: true,
                editar: true,
                eliminar: true
            },
            saldo_tickets: {
                consultar: true,
                crear: true,
                editar: true,
                eliminar: true
            }  
            },
            pacientes: { 
            accesos: true, 
            informacion: {
                consultar: true,
                crear: true,
                editar: true,
                eliminar: true
            },
            casos: {
                consultar: true,
                crear: true,
                editar: true,
                eliminar: true
            },
            productos_servicios: {
                consultar: true,
                crear: true,
                editar: true,
                eliminar: true
            },
            plan_bienestar: {
                consultar: true,
                crear: true,
                editar: true,
                eliminar: true
            }
            },
            casos: {
            accesos: true, 
            casos: {
                consultar: true,
                crear: true,
                editar: true,
                eliminar: true
            },
            productos_servicios: {
                consultar: true,
                crear: true,
                editar: true,
                eliminar: true
            }  
            },
            agenda: {
                accesos: true, 
                citas: {
                    consultar: true,
                    crear: true,
                    editar: true,
                    eliminar: true
                },
                configuracion: {
                    consultar: true,
                    crear: true,
                    editar: true,
                    eliminar: true
                }  
            },
            sala_espera: {
                accesos: true, 
                informacion: {
                    consultar: true,
                    crear: true,
                    editar: true,
                    eliminar: true
                } 
            },
            planes_bienestar: {
            accesos: true, 
            planes_bienestar: {
                consultar: true,
                crear: true,
                editar: true,
                eliminar: true
            } 
            },
            cortes_caja: {
            accesos: true, 
            caja: {
                consultar: true,
                crear: true,
                editar: true,
                eliminar: true
            } 
            },
            descuentos: {
            accesos: true, 
            descuentos: {
                consultar: true,
                crear: true,
                editar: true,
                eliminar: true
            } 
            },
            corporativo: {
            accesos: true, 
            corporativo: {
                consultar: true,
                crear: true,
                editar: true,
                eliminar: true
            },
            franquicias: {
                consultar: true,
                crear: true,
                editar: true,
                eliminar: true
            },
            sucursales: {
                consultar: true,
                crear: true,
                editar: true,
                eliminar: true
            } 
            },
            usuarios: {
                accesos: true, 
                informacion: {
                    consultar: true,
                    crear: true,
                    editar: true,
                    eliminar: true
                }
            },
            roles: {
                accesos: true, 
                informacion: {
                    consultar: true,
                    crear: true,
                    editar: true,
                    eliminar: true
                }
            },
            reportes: {
            accesos: true, 
            reportes: {
                consultar: true,
                crear: true,
                editar: true,
                eliminar: true
            }
            },
            catalogos: {
                accesos: true, 
                catalogos: {
                    consultar: true,
                    crear: true,
                    editar: true,
                    eliminar: true
                }
            },
            autorizacion: {
                accesos: true, 
                informacion: {
                    consultar: true,
                    crear: true,
                    editar: true,
                    eliminar: true
                } 
            },
        }
    }
  }