import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'
import routes from './routers'
var baseUrl = window.location.origin;
import { setToken, getToken, canTurnTo, setTitle } from '@/libs/util'
import config from '@/config'
const { homeName } = config

Vue.use(Router)

const router = new Router({
  linkActiveClass: 'active',
  scrollBehavior: () => ({ y: 0 }),
  mode: 'hash',
  routes,
}); 


const LOGIN_PAGE_NAME = 'FormLogin'

const turnTo = (to, access, next) => {
  if (canTurnTo(to.name, access, routes)) next()
  else next({ replace: true, name: 'error_404' })
}

router.beforeEach((to, from, next) => {
  store.dispatch('modalCargando', true);
  const token = getToken()
  console.log("TCL: token", token)
  if(token){
    /** INICIO VALIDAR ACTIVIDAD EN EL SISTEMA */
    const expirationDuration = 1000 * 60 * 15; // 15 MINUTOS
    const prevAccepted = localStorage.getItem("accepted");
    console.log("TCL: expirationDuration -> prevAccepted", prevAccepted)
    const currentTime = new Date().getTime();
    const notAccepted = prevAccepted == undefined;
    const prevAcceptedExpired = prevAccepted != undefined && currentTime - prevAccepted > expirationDuration
    if (notAccepted || prevAcceptedExpired) {
      console.log("TCL: EL TIMEPO DE LA VARIABLE LOCAL CADUCO");
      
      store.dispatch('handleLogOut').then(res => {
        setTimeout(function(){
          var nombreServidor = document.location.hostname;
          if(nombreServidor == 'localhost'){
            window.location.href = baseUrl;
          }else{
            window.location.href = 'https://serverhm.com/desarrollo/dupont/';
          }
        }, 2000);
        
      });
    }
     /*FIN VALIDAR ACTIVIDAD EN EL SISTEMA */
  }

  if(to.name == 'FormRegistrar' || to.name == 'FormCambioContrasena'
   || to.name == 'Terminos' || to.name == 'Recuperar'){
    /** Inicio de rutas publicas */
    console.log("TCL: 001")
    next()
  }else{
    /** Inicio Rutas protegidas */
    if (!token && to.name !== LOGIN_PAGE_NAME) {
      console.log("TCL: 002")
      store.dispatch('modalCargando', false);
      next({
        name: LOGIN_PAGE_NAME
      })
    } else if (!token && to.name === LOGIN_PAGE_NAME) {
      console.log("TCL: 003")
      next()
    } else if (token && (to.name === LOGIN_PAGE_NAME || to.name === 'Home')) {
      console.log("TCL: 004")

      if (store.state.user.hasGetInfo) {
        if(store.state.user.infoUser.es_registrado){
          if(to.name == 'Solicitudes') next({ name: 'Home' })
          else next()
        }else{
          if(to.name == 'Home') next({ name: 'Solicitudes' })
          else next()
        }
      } else {
        store.dispatch('getUserInfo').then(user => {
          console.log("TCL: user", user)
          if(user.es_registrado){
            if(to.name == 'Solicitudes') next({ name: 'Home' })
            if(to.name == LOGIN_PAGE_NAME) next({ name: 'Home' })
            else next()
          }else{
            if(to.name == 'Home') next({ name: 'Solicitudes' })
            if(to.name == LOGIN_PAGE_NAME) next({ name: 'Solicitudes' })
            else next()
          }
        }).catch(function(error) {
          setToken('')
          next({
            name: 'FormLogin'
          })
        })
      }

    } else {
      console.log("TCL: 005 to.name", to.name)
      if (store.state.user.hasGetInfo) {
        turnTo(to, store.state.user.access, next)
      } else {
        store.dispatch('getUserInfo').then(user => {
          console.log("TCL: datos user desde el router", user)
          turnTo(to, user.access, next)
        }).catch(function(error) {
          console.log("TCL: ERROR")
          setToken('')
          next({
            name: 'FormLogin'
          })
        })
      }
    }/** Fin rutas protegidas */
  } /** Fin de rutas publicas */
})

router.afterEach(to => {
  store.dispatch('modalCargando', false);
  window.scrollTo(0, 0)
})

export default router
