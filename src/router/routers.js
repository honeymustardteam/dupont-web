//  Widgets Components
import mainView from '../mainView'
import error404 from '../views/samples/error-pages/error_404'
import error500 from '../views/samples/error-pages/error_500'


export default [
  {
    path: '/login',
    name: 'FormLogin',
    meta: {
      title: 'Login'
    },
    component: () => import(/* webpackChunkName: "about" */ '../views/Auth/FormLogin.vue')
  },
  {
    path: '/registro',
    name: 'FormRegistrar',
    meta: {
      title: 'Registro'
    },
    component: () => import(/* webpackChunkName: "about" */ '../views/Auth/FormRegistrar.vue')
  },

  
  {
    path: '/cambio/:id',
    name: 'FormCambioContrasena',
    props: true,
    meta: {
      title: 'Cambio'
    },
    component: () => import(/* webpackChunkName: "about" */ '../views/Auth/FormCambioContrasena.vue')
  },
  {
    path: '/recuperar',
    name: 'Recuperar',
    meta: {
      title: 'Recuperar Contraseña'
    },
    component: () => import(/* webpackChunkName: "about" */ '../views/Auth/FormRecuperar.vue')
  },
  {
  path: '/',
  redirect: '/home',
  component: mainView,
  children: [
    {
      path: '/home',
      name: 'Home',
      meta: {
        access: ['registrado'],
        title: 'Inicio'
      },
      component: () => import(/* webpackChunkName: "about" */ '../views/Home/Home.vue')
    },
    {
      path: '/mis-prototipos',
      name: 'MisPototipos',
      meta: {
        access: ['registrado'],
        title: 'Inicio'
      },
      component: () => import(/* webpackChunkName: "about" */ '../views/Prototipos/Prototipos.vue')
    },
    {
      path: '/especificaciones',
      name: 'Especificaciones',
      props: true,
      meta: {
        access: ['registrado'],
        title: 'Especificaciones'
      },
      component: () => import(/* webpackChunkName: "about" */ '../views/NuevoPrototipo/Especificaciones.vue')
    },
    {
      path: '/tipo-de-contenedor',
      name: 'Tipo de Contenedor',
      meta: {
        access: ['registrado'],
        title: 'Tipo de Contenedor'
      },
      component: () => import(/* webpackChunkName: "about" */ '../views/NuevoPrototipo/TipoContenedor.vue')
    },
    {
      path: '/numero-de-piezas',
      name: 'Número de Piezas',
      meta: {
        access: ['registrado'],
        title: 'Número de Piezas'
      },
      component: () => import(/* webpackChunkName: "about" */ '../views/NuevoPrototipo/NumeroPiezas.vue')
    },
    {
      path: '/tipo-de-uso',
      name: 'Tipo de uso',
      meta: {
        access: ['registrado'],
        title: 'Tipo de uso'
      },
      component: () => import(/* webpackChunkName: "about" */ '../views/NuevoPrototipo/TipoUso.vue')
    },
    {
      path: '/almacenamiento',
      name: 'Almacenamiento',
      meta: {
        access: ['registrado'],
        title: 'Almacenamiento'
      },
      component: () => import(/* webpackChunkName: "about" */ '../views/NuevoPrototipo/Almacenamiento.vue')
    },
    {
      path: '/dunnage',
      name: 'Dunnage',
      meta: {
        access: ['registrado'],
        title: 'Dunnage'
      },
      component: () => import(/* webpackChunkName: "about" */ '../views/NuevoPrototipo/Dunnage.vue')
    },
    {
      path: '/resumen',
      name: 'Resumen',
      meta: {
        access: ['registrado'],
        title: 'Resumen'
      },
      component: () => import(/* webpackChunkName: "about" */ '../views/Resumen/Resumen.vue')
    },
    {
      path: '/solicitudes',
      name: 'Solicitudes',
      meta: {
        access: ['registrado'],
        title: 'Solicitudes'
      },
      component: () => import(/* webpackChunkName: "about" */ '../views/Solicitudes/Solicitudes.vue')
    },
    {
      path: '/responsables',
      name: 'Responsables',
      meta: {
        access: ['registrado'],
        title: 'Responsables'
      },
      component: () => import(/* webpackChunkName: "about" */ '../views/Responsables/Responsables.vue')
    },
    

    
  ]
},
{
  path: '*',
  redirect: '/pages/error_404',
  component: {
    render (c) { return c('router-view') }
  },
  children: [
    {
      path: '/pages/error_404',
      name: 'error_404',
      meta: {
        title: 'error'
      },
      component: error404
    },
    {
      path: '/pages/error_500',
      meta: {
        title: 'Error'
      },
      component: error500
    },
  ]}
]
